<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssignmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assignments', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('iddocente')->unsigned();
          $table->integer('idgrado')->unsigned();
          //$table->integer('idasignatura')->unsigned();
          $table->boolean('activo')->default(true);
          $table->foreign('iddocente')->references('id')->on('docentes')->onDelete('cascade');
          $table->foreign('idgrado')->references('id')->on('grados')->onDelete('cascade');
        //  $table->foreign('idasignatura')->references('id')->on('asignaturas')->onDelete('cascade');

          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assignments');
    }
}
