<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaArea2v1 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('area2s', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('idrecord')->unsigned();
            $table->enum('I1', ['S', 'T', 'P'])->nullable();
            $table->enum('I2', ['S', 'T', 'P'])->nullable();
            $table->enum('I3', ['S', 'T', 'P'])->nullable();
            $table->enum('I4', ['S', 'T', 'P'])->nullable();
            $table->enum('I5', ['S', 'T', 'P'])->nullable();
            $table->enum('I6', ['S', 'T', 'P'])->nullable();
            $table->enum('I7', ['S', 'T', 'P'])->nullable();
            $table->enum('I8', ['S', 'T', 'P'])->nullable();
            $table->enum('I9', ['S', 'T', 'P'])->nullable();
            $table->enum('I10', ['S', 'T', 'P'])->nullable();
            $table->enum('I11', ['S', 'T', 'P'])->nullable();
            $table->enum('I12', ['S', 'T', 'P'])->nullable();
            $table->enum('I13', ['S', 'T', 'P'])->nullable();
            $table->enum('I14', ['S', 'T', 'P'])->nullable();
            $table->enum('I15', ['S', 'T', 'P'])->nullable();
            $table->enum('nivel', ['Inicio', 'Intermedio', 'Final'])->default('Inicio');
            $table->integer('anio');
            $table->foreign('idrecord')->references('id')->on('records')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('area2');
    }
}
