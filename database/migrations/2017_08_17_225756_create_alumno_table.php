<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAlumnoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {
        Schema::create('alumnos', function (Blueprint $table) {
          $table->increments('id');
          $table->string('nombres', 100);
          $table->string('apellido_padre', 50);
          $table->string('apellido_madre', 50);
          $table->enum('sexo', ['Femenino', 'Masculino']);
          $table->date('fecha_nacimiento');
          $table->enum('repite', ['Si', 'No'])->default('No');
          $table->enum('estudio_parvularia', ['Si', 'No'])->default('Si');
          $table->enum('zona_residencia', ['Rural', 'Urbana']);
          $table->string('cod_depto_residencia', 4);
          $table->enum('actividad_econ', ['Si', 'No'])->default('No');
          $table->string('tipos_discapacidad', 100);
          $table->string('encargado', 100);
          $table->string('direccion', 100);
          $table->boolean('activo')->default(true);
          $table->boolean('asignado')->default(false);          

          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alumnos');
        //
    }
}
