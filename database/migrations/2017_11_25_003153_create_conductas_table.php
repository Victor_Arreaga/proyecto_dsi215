<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConductasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('conductas', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('idrecord')->unsigned();
          $table->string('c1', 60);
          $table->string('c2', 60);
          $table->string('c3', 60);
          $table->string('c4', 60);
          $table->string('c5', 60);    
          $table->integer('idtrimestre')->unsigned();
          $table->foreign('idrecord')->references('id')->on('records')->onDelete('cascade');
          $table->foreign('idtrimestre')->references('id')->on('trimestres')->onDelete('cascade');
          //$table->boolean('activo')->default(true);
          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('conductas');
    }
}
