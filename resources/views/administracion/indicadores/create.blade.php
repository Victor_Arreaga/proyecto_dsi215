@extends('templates.main')

@section('title', 'Registro de Indicadores')

@section('content')
	{!! Form::open(['route'=>'indicadors.store', 'method'=>'POST']) !!}
		<div class="form-group">
			{!! Form::label('id_area', 'Area Asignada') !!}
			{!! Form::select('id_area', $area_indicadors, null, ['class'=>'form-control', 'placeholder'=>'Seleccione un area','required']) !!}
						
		</div>

		<div class="form-group">
		{!! Form::label('nombre', 'Nombre de Indicador') !!}

		{!! Form::text('nombre', null, ['class'=>'form-control', 'placeholder'=>'Escriba el nombre del indicador', 'required']) !!}
		</div>	
		<div class="form-group">
		{!! Form::label('Nivel', 'Nivel Asignado') !!}
			{!! Form::select('Nivel', [''=>'Selecione','5'=>'5 Años', '6'=>'6 Años'], null,['class'=>'form-control']) !!}
			
		</div>	
		<div class="form-group">
			{!! Form::submit('Registrar', ['class'=>'btn btn-primary']) !!}
		</div>
	{!! Form::close() !!}
@endsection