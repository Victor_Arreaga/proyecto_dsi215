@extends('templates.main')

@section('title', 'Asignacion de Grado a Asignatura')

@section('content')
	{!! Form::open(['route'=>'atribuciones.store', 'method'=>'POST']) !!}
		<div class="form-group">
			{!! Form::label('idgrado', 'Listado de Grados') !!}
			{!! Form::select('idgrado', $grados, null, ['class'=>'form-control', 'placeholder'=>'Seleccione un Grado','required']) !!}
		</div>

		<div class="form-group">
			{!! Form::label('idasignatura', 'Listado de Asignaturas') !!}
			{!! Form::select('idasignatura', $asig, null, ['class'=>'form-control' , 'placeholder'=>'Seleccione una Asignatura','required']) !!}
		</div>


		<div class="form-group">
			{!! Form::submit('Registrar', ['class'=>'btn btn-primary']) !!}

			<a href="{{ route('atribuciones.index') }}" class="btn btn-info">Atras</a>
		</div>
	{!! Form::close() !!}
@endsection
