@extends('templates.main')

@section('title', 'Registro de Notas por Materia')

@section('content')

    <h3 align="center">Registro de {{$identificador->nombre}}</h3>
    <h3 align="center">Conducta</h3>
    <h3 align="center">Tercer Trimestre</h3>
<div align="RIGHT">
    <TABLE BORDER=2 bordercolor="red" align="RIGHT">
	     <TR>
         <TD><b><big>1</b></TD>
         <TD>Se respeta a sí mismo (a) y a los demás.</TD>
	    </TR>
	    <TR>
        <TD><b><big>2</b></TD>
        <TD>Convive de forma armónica y solidaria.</TD>
	   </TR>
     <TR>
       <TD><b><big>3</b></TD>
       <TD>Toma decisiones responsablemente.</TD>
    </TR>
    <TR>
      <TD><b><big>4</b></TD>
      <TD>Cumple los deberes y ejerce correctamente sus derechos.</TD>
   </TR>
   <TR>
     <TD><b><big>5</b></TD>
     <TD>Practica valores morales y cívicos.</TD>
  </TR>
   </TABLE>
</div>
</hr>

    {!! Form::open(['route'=>['conductas.update', $identificador], 'method'=>'PUT']) !!}
    <td align="center">
    <a href="{{ url('administracion/conductas/editar', ["idgrado" => $identificador->id]) }}"  class="btn btn-warning">
      <span class="glyphicon glyphicon-search"  aria-hidden="true">Primer_Trimestre</span>
    </a>
    </td>
    <td align="center">
    <a href="{{ url('administracion/conductas/edit_conducta', ["idgrado" => $identificador->id]) }}"  class="btn btn-warning">
      <span class="glyphicon glyphicon-search"  aria-hidden="true">Segundo_Trimestre</span>
    </a>
    </td>

    <table class="table table-condensed">

        <thead>
          <th>Nombres</th>
          <th>Apellidos</th>
          <th></th>
          <th>1</th>
          <th>2</th>
          <th>3</th>
          <th>4</th>
          <th>5</th>
          <th></th>

          <th></th>

        </thead>

        <tbody>


            @foreach($registros as $registro)
               @if($registro->idgrado == $identificador->id)
							  @if ($registro->activo == 1)
                   <tr>
                     <td style="width:130px;">{{ $registro->alumnos->nombres }}</td>
                     <td style="width:130px;">{{ $registro->alumnos->apellido_padre }} {{ $registro->alumnos->apellido_madre }}</td>
  {!! Form::close() !!}

{!! Form::open(['route'=>'conductas.store', 'method'=>'POST']) !!}
<td>
<div class="form-group">
  {!!  Form::hidden('idrecord', $registro->id)!!}
</div>
</td>
<td style="width:100px;">
<div class="form-group">
 {!! Form::select('c1', [''=>'-','Excelente'=>'E', 'Muy Bueno'=>'MB', 'Bueno'=>'B'], null, ['class'=>'form-control']) !!}
</div>
</td>
<td style="width:100px;">
<div class="form-group">
 {!! Form::select('c2', [''=>'-','Excelente'=>'E', 'Muy Bueno'=>'MB', 'Bueno'=>'B'], null, ['class'=>'form-control']) !!}
</div>
</td>
<td style="width:100px;">
<div class="form-group">
 {!! Form::select('c3', [''=>'-','Excelente'=>'E', 'Muy Bueno'=>'MB', 'Bueno'=>'B'], null, ['class'=>'form-control']) !!}
</div>
</td>
<td style="width:100px;">
<div class="form-group">
 {!! Form::select('c4', [''=>'-','Excelente'=>'E', 'Muy Bueno'=>'MB', 'Bueno'=>'B'], null, ['class'=>'form-control']) !!}
</div>
</td>
<td style="width:100px;">
<div class="form-group">
 {!! Form::select('c5', [''=>'-','Excelente'=>'E', 'Muy Bueno'=>'MB', 'Bueno'=>'B'], null, ['class'=>'form-control']) !!}
</div>
</td>
<td>
<div class="form-group">
  {!!  Form::hidden('idtrimestre', '3')!!}
</div>
</td>
                <td><div class="form-group">
                    		{!! Form::submit('Registrar', ['class'=>'btn btn-primary']) !!}
                    	</div></td>
{!! Form::close() !!}
<!--<td align="center">
<a href="{{ route('administracion.notas.edit_historial',["idgrado" => $registro->grados->id, "idalumno" => $registro->alumnos->id]) }}"  class="btn btn-warning">
  <span class="glyphicon glyphicon-search"  aria-hidden="true">Historial</span>
</a>
</td>-->
                  </tr>

								@endif
             @endif
             @endforeach

        </tbody>
	</table>




@endsection
