@extends('templates.main')

@section('title', 'Asignacion de Docente a Grado')

@section('content')
	{!! Form::open(['route'=>'asignaciones.store', 'method'=>'POST']) !!}

	<div class="form-group">
		{!! Form::label('iddocente', 'Lista de Docentes') !!}
		{!! Form::select('iddocente',  $docentes, null, ['class'=>'form-control', 'placeholder'=>'Seleccione un Docente', 'required']) !!}
	</div>

  <div class="form-group">
    {!! Form::label('idgrado', 'Lista de Grados') !!}
    {!! Form::select('idgrado',  $grados, null, ['class'=>'form-control', 'placeholder'=>'Seleccione un Grado', 'required']) !!}
  </div>



	<div class="form-group">
		{!! Form::submit('Registrar', ['class'=>'btn btn-primary']) !!}
		<a href="{{ route('asignaciones.index') }}" class="btn btn-info">Cancelar</a>

	</div>

	{!! Form::close() !!}
@endsection
