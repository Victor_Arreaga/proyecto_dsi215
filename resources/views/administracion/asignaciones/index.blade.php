@extends('templates.main')

@section('title', 'Asignacion de Grados a Docentes')

@section('content')
    <a href="{{ route('asignaciones.create') }}" class="btn btn-info">Asignar un nuevo Docente a un Grado</a>
   
    <a href="{{ route('basig.index') }}" class="btn btn-info">Consultar Asignaciones Inactivas</a>
     <a href="{{ route('home') }}" class="btn btn-info">Inicio</a>
  <hr>
   <div class="table table-responsive">
    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
        <thead>

            <th>Docente</th>
            <th>Grado</th>
            <th>Opciones</th>

        </thead>
        <tbody>
          @foreach($asigna as $asignacion)
           @if ($asignacion->activo == 1)
           <tr>
             <td>{{ $asignacion->docentes->nombres }} , {{ $asignacion->docentes->apellidos }}</td>
             <td>{{ $asignacion->grados->nombre }} ( {{ $asignacion->grados->turnos->turno }} )</td>
             <td align="center
             "><a href="{{ route('administracion.asignaciones.destroy', $asignacion->id) }}" onclick="return confirm('¿Desea dar de baja a este Registro?')" class="btn btn-danger"><span class="glyphicon glyphicon-arrow-down" aria-hidden="true"></span></a>
               <a href="{{ route('asignaciones.edit', $asignacion->id) }}"  class="btn btn-warning"><span class="glyphicon glyphicon-wrench"  aria-hidden="true"></span></a></td>
          </tr>
          @endif
         @endforeach

        </tbody>
	</table>
</div>>
@endsection
