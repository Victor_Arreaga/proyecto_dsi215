@extends('templates.main')

@section('title', 'Asignacion de Alumno a Grado')

@section('content')
	{!! Form::open(['route'=>'registros.store', 'method'=>'POST']) !!}
		<div class="form-group">
			{!! Form::label('idalumno', 'Listado de alumnos') !!}
			{!! Form::select('idalumno', $alumnos, null, ['class'=>'form-control', 'placeholder'=>'Seleccione un Alumno','required']) !!}
		</div>

		<div class="form-group">
			{!! Form::label('idgrado', 'Listado de Asignaciones de Grados/Docentes') !!}
			{!! Form::select('idgrado', $asig, null, ['class'=>'form-control', 'placeholder'=>'Seleccione un Grado','required']) !!}
		</div>



		<div class="form-group">
			{!! Form::submit('Registrar', ['class'=>'btn btn-primary']) !!}
		</div>
	{!! Form::close() !!}
@endsection
