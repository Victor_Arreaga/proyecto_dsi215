@extends('templates.main')

@section('title', 'Asignacion de Alumno')

@section('content')
	{!! Form::open(['route'=>['registros.update', $identificador], 'method'=>'PUT']) !!}
  <div class="form-group">
    {!! Form::label('idalumno', 'Listado de alumnos') !!}
    {!! Form::select('idalumno', $alumnos, $identificador->idalumno, ['class'=>'form-control', 'placeholder'=>'Seleccione un Alumno','required']) !!}
  </div>
	<div class="form-group">
		{!! Form::label('idgrado', 'Grado') !!}

		{!! Form::select('idgrado', $grados, $identificador->idgrado, ['class'=>'form-control', 'placeholder'=>'Seleccione un Grado', 'required']) !!}
	</div>



	<div class="form-group">
		{!! Form::submit('Actualizar', ['class'=>'btn btn-primary']) !!}

	</div>
	{!! Form::close() !!}
@endsection
