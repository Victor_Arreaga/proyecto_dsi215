@extends('templates.main')

@section('title', 'Alumnos por Grado Asignado')

@section('content')
    <a href="{{ route('alumnos.create') }}" class="btn btn-info">Matricular un nuevo Alumno</a>

     <hr>
    <div class="table-responsive">
    <table class="table table-striped table-bordered table-hover">
        <thead>
            <th>ID</th>
            <th>Grados Asignados</th>
            <th align="right">Listados de Alumnos</th>

        </thead>
        <tbody>
          @foreach($registros as $registro)
             @if($registro->iddocente == Auth::user()->iddocente)
             <tr>
                 <td>{{ $registro->grados->id }}</td>
                 <td>{{ $registro->grados->nombre }}</td>

                 <td align="rigth">
                 <a href="{{ route('registros.edit', $registro->idgrado) }}"  class="btn btn-warning">
                   <span class="glyphicon glyphicon-search"  aria-hidden="true"></span>
                 </a>
                 </td>

             </tr>
             @endif

             @endforeach

        </tbody>
	</table>
     {{$registros->render()}}
@endsection
