@extends('templates.main')

@section('title', 'Actualización de datos de docente')

@section('content')
	{!! Form::open(['route'=>['docentes.update', $docente], 'method'=>'PUT']) !!}
	<div class="form-group">
		{!! Form::label('nip', 'Número de Identificación Profesional') !!}

		{!! Form::text('nip', $docente->nip, ['class'=>'form-control', 'placeholder'=>'#######', 'required',]) !!}
	</div>
	<div class="form-group">
		{!! Form::label('nombres', 'Nombres') !!}

		{!! Form::text('nombres', $docente->nombres, ['class'=>'form-control', 'placeholder'=>'Escriba sus nombres', 'required']) !!}
	</div>
	
	<div class="form-group">
		{!! Form::label('apellidos', 'Apellidos') !!}

		{!! Form::text('apellidos', $docente->apellidos, ['class'=>'form-control', 'placeholder'=>'Escriba sus apellidos', 'required']) !!}
	</div>

	<div class="form-group">
		{!! Form::label('dui', 'Documento Único de Identidad') !!}

		{!! Form::text('dui', $docente->dui, ['class'=>'form-control', 'placeholder'=>'########-#', 'required']) !!}
	</div>

	<div class="form-group">
		{!! Form::label('nit', 'Número de Identificación Tributaria') !!}

		{!! Form::text('nit', $docente->nit, ['class'=>'form-control', 'placeholder'=>'####-######-###-#', 'required',]) !!}
	</div>

	<div class="form-group">
		{!! Form::label('especialidad', 'Especialidad') !!}

		{!! Form::text('especialidad', $docente->especialidad, ['class'=>'form-control', 'placeholder'=>'Especialidad o grado académico del docente', 'required',]) !!}
	</div>

	<div class="form-group">
		{!! Form::submit('Actualizar', ['class'=>'btn btn-primary']) !!}
		<a href="{{ route('docentes.index') }}" class="btn btn-info">Cancelar</a><hr>
		
	</div>
	{!! Form::close() !!}
@endsection