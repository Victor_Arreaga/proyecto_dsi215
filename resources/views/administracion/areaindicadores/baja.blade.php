@extends('templates.main')

@section('title', 'Listado de Area de Indicadores Inactivas')

@section('content')

    <a href="{{ route('area_indicadores.index') }}" class="btn btn-info right">Consultar Areas Activas</a>
    
    
   <div class="table table-responsive">
    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
        <thead>
      		<th>Nombre</th>
          <th>Acción</th>
        </thead>
        <tbody>
                     @foreach($area_indicadores as $area_indicador)                                 
                     @if ($area_indicador->activo == 0)
                <tr>
                   
                  <td>{{ $area_indicador->nombre }}</td>
                  <td align="center"><a href="{{ route('administracion.area_indicadores.alta', $area_indicador->id) }}" onclick="return confirm('¿Deseas dar de alta esta Area?')" class="btn btn-danger"><span r"><span class="glyphicon glyphicon-arrow-down" aria-hidden="true"></span></a>                    
                </tr>
    @endif
                  
                
            @endforeach
        </tbody>
	</table>
   {{$area_indicadores->render()}}
  @endsection