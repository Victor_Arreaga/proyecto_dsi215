@extends('templates.main')

@section('title', 'Listado de Areas ')

@section('content')
    <a href="{{ route('area_indicadores.create') }}" class="btn btn-info right">Registrar nueva Area Indicador</a>
    <a href="{{ route('area_indicadores.baja.index') }}" class="btn btn-info right">Consultar Areas Inactivas</a>
    <a href="{{ route('home') }}" class="btn btn-info right">Inicio</a><hr>


    <div class="table table-responsive">
    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
        <thead>
      		<th>Nombre</th>
          <th>Acción</th>
        </thead>
        <tbody>
                     @foreach($area_indicadores as $area_indicador)
                      @if ($area_indicador->activo == 1)
                <tr>
                  
    <td>{{ $area_indicador->nombre }}</td>
                  <td align="center"><a href="{{ route('administracion.area_indicadores.destroy', $area_indicador->id) }}"  onclick="return confirm('¿Deseas dar de baja esta Area?')" class="btn btn-danger"><span r"><span class="glyphicon glyphicon-arrow-down" aria-hidden="true"></span></a>
                    <a href="{{ route('area_indicadores.edit', $area_indicador->id) }}"  class="btn btn-warning"><span class="glyphicon glyphicon-wrench"  aria-hidden="true"></span></a></td>
                </tr>
    @endif


            @endforeach
        </tbody>
	</table>
  {{$area_indicadores->render()}}
  @endsection
