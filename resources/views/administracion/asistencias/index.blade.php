@extends('templates.main')

@section('title', 'Registro de Asistencia')

@section('content')


    
    <a href="{{ route('home') }}" class="btn btn-info">Inicio</a>

    <hr>
    <div class="table table-responsive">
    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
        <thead>
            <th>Grados</th>
            <th>Turno</th>
            <th align="rigth">Registrar Asistencia</th>

        </thead>
        <tbody>
          @foreach($registros as $registro)
             @if($registro->iddocente == Auth::user()->iddocente)
             <tr>
                 <td>{{ $registro->grados->nombre }}</td>
                 <td>{{ $registro->grados->turnos->turno }}</td>

                 <td align="rigth">
                 <a href="{{ route('asistencias.edit', $registro->grados->id) }}"  class="btn btn-warning">
                   <span   aria-hidden="true">Registrar Asistencia</span>
                 </a>
                 </td>

             </tr>
             @endif

             @endforeach

        </tbody>
	</table>
     
@endsection