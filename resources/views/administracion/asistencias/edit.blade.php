@extends('templates.main')

@section('title', 'Registro de Asistencia')

@section('content')
 
    <h3>Asistencia del Grado: {{$identificador->nombre}}</h3>
    <hr>

    
  

  <div class="container" style="margin-top: 10px;">
    <div class="row">
      <div class="col-md-8">
        <form action="{{ url('administracion/ingresar-asistencia') }}" enctype="multipart/form-data">
        {!! csrf_field() !!}

        <div class="form-group">
    <input type="date" name="fecha">
    </div>



  <td><input type="hidden" name="grado" value="{{$grado}}" readonly></td> 






   <div class="table table-responsive">
    <table class="table table-striped table-bordered table-hover" id="dataTables-example">

        <thead>
          <th></th>
          <th>Nombres</th>
          <th>Apellidos</th>
          <th>Estado</th>


        </thead>

        <tbody>

            @foreach($registros as $registro)
               @if($registro->idgrado == $identificador->id)
							  @if ($registro->activo == 1)
                   <tr>
                     <td><input type="hidden" name="record[]" value="{{$registro->id}}" readonly></td> 
                     <td>{{ $registro->alumnos->nombres }}</td>
                     <td>{{ $registro->alumnos->apellido_padre }} {{ $registro->alumnos->apellido_madre }}</td>

                    <td ><select name="estado[]">
             
              <option value="Asistio">Asistio</option>
              <option value="Permiso">Permiso</option>
              <option value="Sin Permiso">Sin Permiso</option>
               <option value="Asueto">Asueto</option>
                  </select></td>
                  </tr>
								@endif
             @endif
             @endforeach

        </tbody>
	</table>
  <div class="form-group">
          <button class="btn btn-primary btn-sm">
            Ingresar
          </button>          
        </div>
        </form>
      </div>
    </div>
  </div>


@endsection


