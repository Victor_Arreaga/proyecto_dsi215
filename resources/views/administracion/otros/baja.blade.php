@extends('templates.main')

@section('title', 'Asignacion de Alumno a Grado')

@section('content')
	
	<div class="form-group">
		{!! Form::submit('Asignar', ['class'=>'btn btn-primary']) !!}
    <a href="{{ route('otros.index') }}" class="btn btn-info">Atras</a>
	</div>
	{!! Form::close() !!}

     <hr>
    <h3>Datos de Alumnos de: {{$identificador->nombre}}</h3>
   <div class="table table-responsive">
    <table class="table table-striped table-bordered table-hover" id="dataTables-example">

        <thead>
          <th>Nombres</th>
          <th>Apellidos</th>
          <th>Opciones</th>


        </thead>

        <tbody>

            @foreach($registros as $registro)
               @if($registro->idgrado == $identificador->id)
							  @if ($registro->activo == 1)
                   <tr>
                     <td>{{ $registro->alumnos->nombres }}</td>
                     <td>{{ $registro->alumnos->apellido_padre }} {{ $registro->alumnos->apellido_madre }}</td>

                     <td>
                       <a href="{{ route('administracion.otros.destroy', $registro->id) }}" onclick="return confirm('¿Deseas dar de baja a este Registro?')" class="btn btn-danger">
                        <span class="glyphicon glyphicon-arrow-down" aria-hidden="true"></span>
                       </a>
                       <a href="{{ route('registros.edit', $registro->id) }}"  class="btn btn-warning">
                        <span class="glyphicon glyphicon-wrench"  aria-hidden="true"></span></a>
                    </td>
                  </tr>
								@endif
             @endif
             @endforeach

        </tbody>
	</table>
@endsection
