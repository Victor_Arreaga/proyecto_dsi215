@extends('templates.main')

@section('title', 'Nomina de Alumnos de Parvularia')

@section('content')
<h4 align="center">Prof: {{$data0}} {{$data1}}</h4>
<h4 align="center">Indicadores de Logro por Areas de {{$data3}}</h4>
<h4 align="center">{{$data2}}         {{$year}}</h4>
<hr>
<div align="CENTER">
    <TABLE BORDER=2 bordercolor="red" align="CENTER">
     <TR>
       <TD><b><big>I1</b></TD>
       <TD>Se diferencia como niño o niña y se describe por atributos físicos sin discriminación de género.</TD>
      </TR>
     <TR>
        <TD><b><big>I2</b></TD>
        <TD>Se sostiene en un pie por cinco segundos y los brazos extendidos.</TD>
     </TR>
     <TR>
       <TD><b><big>I3</b></TD>
       <TD>Camina hacia atrás sobre una tabla.</TD>
    </TR>
    <TR>
      <TD><b><big>I4</b></TD>
      <TD>Corre y puede disminuir velocidad, recoger un objeto y continuar.</TD>
   </TR>
   <TR>
     <TD><b><big>I5</b></TD>
     <TD>Camina sobre una tabla inclinada.</TD>
  </TR>
  <TR>
     <TD><b><big>I6</b></TD>
     <TD>Salta en un pie con el otro arriba.</TD>
  </TR>
  <TR>
     <TD><b><big>I7</b></TD>
     <TD>Sube y baja rápidamente las gradas.</TD>
  </TR>
  <TR>
     <TD><b><big>I8</b></TD>
     <TD>Se para en puntillas con los ojos abiertos.</TD>
  </TR>
  <TR>
     <TD><b><big>I9</b></TD>
     <TD>Puede patear un objeto pequeño en forma consecutiva.</TD>
  </TR>
  <TR>
     <TD><b><big>I10</b></TD>
     <TD>Lanza pelota con un bate(de plástico).</TD>
  </TR>
  <TR>
     <TD><b><big>I11</b></TD>
     <TD>Ataja ocasionalmente la pelota pequeña.</TD>
  </TR>
  <TR>
       <TD><b><big>I12</b></TD>
       <TD>Utilizar la lateralidad derecha o izquierda de su cuerpo según indicación.</TD>
     </TR>
     <TR>
       <TD><b><big>I13</b></TD>
       <TD>Rasga una figura curva</TD>
    </TR>
    <TR>
      <TD><b><big>I14</b></TD>
      <TD>Recorta de manera imperfecta respetando líneas curvas, ángulos y rectas.</TD>
   </TR>
   <TR>
     <TD><b><big>I15</b></TD>
     <TD>Elabora grafismos diversos con direccionalidad e intención comunicativa.</TD>
  </TR>
  <TR>
     <TD><b><big>I16</b></TD>
     <TD>Disfruta tomar decisiones y resolver conflictos con otros niños y otras niñas</TD>
  </TR>
  <TR>
     <TD><b><big>I17</b></TD>
     <TD>Se viste sin ayuda.</TD>
  </TR>
  <TR>
     <TD><b><big>I18</b></TD>
     <TD>Se baña solo sin supervisión.</TD>
  </TR>
  <TR>
     <TD><b><big>I19</b></TD>
     <TD>Inventa juegos de roles por iniciativa.</TD>
  </TR>
  <TR>
     <TD><b><big>I20</b></TD>
     <TD>Interactúa de manera espontánea con otros niños y otras niñas de su edad.</TD>
  </TR>
  <TR>
     <TD><b><big>I21</b></TD>
     <TD>Dice por favor y gracias</TD>
  </TR>
   </TABLE>
</div>

<div class="container" style="margin-top: 10px;">
    <div class="row">
      <div class="col-md-8">
        <form action="{{ url('administracion/multiple-inserting') }}" enctype="multipart/form-data">
        {!! csrf_field() !!}



<div class="table-responsive">
  <table class="table">
  <thead>
  <th></th>
  </thead>
  <tbody>          
            @php($i=1)
            @foreach($area1 as $are1)
            @if($are1->nivel == $data2)
              <td><input type="hidden" name="hidden[]" value="{{$are1->id}}" readonly></td> 
            @php($i++)
            @endif
            @endforeach 
          </tbody>
    
  </table>
</div>


<div class="table-responsive">
  <table class="table">
  <thead>
  <tr>
              <th>Nombre</th>
              <th>Apellidos</th>
              <th>I1</th>
              <th>I2</th>
              <th>I3</th>
              <th>I4</th>
              <th>I5</th>
              <th>I6</th>
              <th>I7</th>
              <th>I8</th>
              <th>I9</th>
              <th>I10</th>
              <th>I11</th>
              <th>I12</th>
              <th>I13</th>
              <th>I14</th>
              <th>I15</th>
              <th>I16</th>
              <th>I17</th>
              <th>I18</th>
              <th>I19</th>
              <th>I20</th>
              <th>I21</th>
            </tr>
  </thead>
  <tbody>
   @php($j=1)
            @foreach($area1 as $are1)
            @if($are1->nivel == $data2)
              <td>{{$are1->record->alumnos->nombres}}</td>
              <td>{{$are1->record->alumnos->apellido_padre}}</td>
              <td ><select name="avance1[]">
              <option value="{{ $are1->I1}}">{{$are1->I1}}</option>
              <option value="S">S</option>
              <option value="T">T</option>
              <option value="P">P</option>
              </select></td>

              <td ><select name="avance2[]">
              <option value="{{ $are1->I2}}">{{$are1->I2}}</option>
              <option value="S">S</option>
              <option value="T">T</option>
              <option value="P">P</option>
              </select></td>


              <td ><select name="avance3[]">              
              <option value="{{ $are1->I3}}">{{$are1->I3}}</option>
              <option value="S">S</option>
              <option value="T">T</option>
              <option value="P">P</option>
              </select></td>

              <td ><select name="avance4[]">
              <option value="{{ $are1->I4}}">{{$are1->I4}}</option>              
              <option value="S">S</option>
              <option value="T">T</option>
              <option value="P">P</option>
              </select></td>

              <td ><select name="avance5[]">
              <option value="{{ $are1->I5}}">{{$are1->I5}}</option>              
              <option value="S">S</option>
              <option value="T">T</option>
              <option value="P">P</option>
              </select></td>

              <td ><select name="avance6[]">
              <option value="{{ $are1->I6}}">{{$are1->I6}}</option>
              <option value="S">S</option>
              <option value="T">T</option>
              <option value="P">P</option>
              </select></td>

              <td ><select name="avance7[]">
              <option value="{{ $are1->I7}}">{{$are1->I7}}</option>
              <option value="S">S</option>
              <option value="T">T</option>
              <option value="P">P</option>
              </select></td>

              <td ><select name="avance8[]">
              <option value="{{ $are1->I8}}">{{$are1->I8}}</option>
              <option value="S">S</option>
              <option value="T">T</option>
              <option value="P">P</option>
              </select></td>

              <td ><select name="avance9[]">
              <option value="{{ $are1->I9}}">{{$are1->I9}}</option>
              <option value="S">S</option>
              <option value="T">T</option>
              <option value="P">P</option>
              </select></td>

              <td ><select name="avance10[]">
              <option value="{{ $are1->I10}}">{{$are1->I10}}</option>
              <option value="S">S</option>
              <option value="T">T</option>
              <option value="P">P</option>
              </select></td>

              <td ><select name="avance11[]">
              <option value="{{ $are1->I11}}">{{$are1->I11}}</option>
              <option value="S">S</option>
              <option value="T">T</option>
              <option value="P">P</option>
              </select></td>

              <td ><select name="avance12[]">
              <option value="{{ $are1->I12}}">{{$are1->I12}}</option>
              <option value="S">S</option>
              <option value="T">T</option>
              <option value="P">P</option>
              </select></td>

              <td ><select name="avance13[]">
              <option value="{{ $are1->I13}}">{{$are1->I13}}</option>
              <option value="S">S</option>
              <option value="T">T</option>
              <option value="P">P</option>
              </select></td>

              <td ><select name="avance14[]">
              <option value="{{ $are1->I14}}">{{$are1->I14}}</option>
              <option value="S">S</option>
              <option value="T">T</option>
              <option value="P">P</option>
              </select></td>

              <td ><select name="avance15[]">
              <option value="{{ $are1->I15}}">{{$are1->I15}}</option>
              <option value="S">S</option>
              <option value="T">T</option>
              <option value="P">P</option>
              </select></td>

              <td ><select name="avance16[]">
              <option value="{{ $are1->I16}}">{{$are1->I16}}</option>
              <option value="S">S</option>
              <option value="T">T</option>
              <option value="P">P</option>
              </select></td>
              <td ><select name="avance17[]">
              <option value="{{ $are1->I17}}">{{$are1->I17}}</option>
              <option value="S">S</option>
              <option value="T">T</option>
              <option value="P">P</option>
              </select></td>
              <td ><select name="avance18[]">
              <option value="{{ $are1->I18}}">{{$are1->I18}}</option>
              <option value="S">S</option>
              <option value="T">T</option>
              <option value="P">P</option>
              </select></td>

              <td ><select name="avance19[]">
              <option value="{{ $are1->I19}}">{{$are1->I19}}</option>
              <option value="S">S</option>
              <option value="T">T</option>
              <option value="P">P</option>
              </select></td>

              <td ><select name="avance20[]">
              <option value="{{ $are1->I20}}">{{$are1->I20}}</option>
              <option value="S">S</option>
              <option value="T">T</option>
              <option value="P">P</option>
              </select></td>

              <td ><select name="avance21[]">
              <option value="{{ $are1->I21}}">{{$are1->I21}}</option>
              <option value="S">S</option>
              <option value="T">T</option>
              <option value="P">P</option>
              </select></td>

            </tr>
            @php($j++)
            @endif
            @endforeach
    
  </tbody>
    
  </table>
  <td><input type="hidden" name="identificador" value="{{$data3}}" readonly></td> 
</div> 
<hr>

        <div class="form-group">
          <button class="btn btn-primary btn-sm">
            Guardar Indicadores
          </button>          
        </div>
        </form>
      </div>
    </div>
  </div>

  @endsection