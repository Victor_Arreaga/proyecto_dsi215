@extends('templates.main')

@section('title', 'Registros de Indicadores de Parvularia Y Conteo Final por Alumno')

@section('content')
<h4 align="center">Seleccionar Parametros de Registros de Indicadores</h4>
<hr>
    <div class="table-responsive">
    <form action="{{ url('administracion/cargar-formulario') }}" enctype="multipart/form-data">
        {!! csrf_field() !!}
    <table class="table table-striped table-bordered table-hover">
        <thead>
            <th>Grado</th>
            <th>Area</th>
            <th>Nivel</th>
        </thead>                
        <tbody>
            <tr>
              <td><select name="grade">
              @foreach($asignas as $asigna)
              @if($asigna->iddocente == Auth::user()->iddocente)
              <option value="{{ $asigna->idgrado}}">{{$asigna->grados->nombre}}</option>
              @endif
              @endforeach
              </select></td>

              <td><select name="area">
              @foreach($areas as $area)
              <option value="{{ $area}}">{{$area}}</option>
              @endforeach
              </select></td>

              <td><select name="level">
              @foreach($a as $aa)<option value="{{ $aa}}">{{$aa}}</option>
              @endforeach
              </select></td>            
            </tr>
        </tbody>
    </table>
    <td><input type="hidden" name="docente" value="{{Auth::user()->iddocente}}" readonly></td> 
    <div class="form-group">
          <button class="btn btn-primary btn-sm">Cargar Formulario</button>
    </div>
    </form>
    </div>

    <h4 align="center">Seleccionar Grado para el Conteo de Indicadores</h4>
    <hr>
    <div class="table-responsive">
    <form action="{{ url('administracion/cargar-registros') }}" enctype="multipart/form-data">
        {!! csrf_field() !!}
    <table class="table table-striped table-bordered table-hover" align="center">
        <thead>
            <th>Grado</th>
        </thead>                
        <tbody>
            <tr>
              <td><select name="grade">
              @foreach($asignas as $asigna)
              @if($asigna->iddocente == Auth::user()->iddocente)
              <option value="{{ $asigna->idgrado}}">{{$asigna->grados->nombre}}</option>
              @endif
              @endforeach
              </select></td>            
            </tr>
        </tbody>
    </table>
    <td><input type="hidden" name="docente" value="{{Auth::user()->iddocente}}" readonly></td> 
    <div class="form-group">
          <button class="btn btn-primary btn-sm">Historico</button>
    </div>
    </form>
    </div>
        
@endsection