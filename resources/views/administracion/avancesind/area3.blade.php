@extends('templates.main')

@section('title', 'Nomina de Alumnos de Parvularia')

@section('content')
<h4 align="center">Prof: {{$data0}} {{$data1}}</h4>
<h4 align="center">Indicadores de Logro por Areas de {{$data3}}</h4>
<h4 align="center">{{$data2}}         {{$year}}</h4>
<hr>
<div align="CENTER">
    <TABLE BORDER=2 bordercolor="red" align="CENTER">
     <TR>
       <TD><b><big>I1</b></TD>
       <TD>Ordena una serie de objetos que varían por tamaño, de la más pequeña a las más grande, señalando el primero y el último.</TD>
      </TR>
     <TR>
        <TD><b><big>I2</b></TD>
        <TD>Identifica en la relación con objetos: arriba-abajo, delante-detrás, dentro-fuera, encima, debajo, cerca-lejos.</TD>
     </TR>
     <TR>
       <TD><b><big>I3</b></TD>
       <TD>Inicia la identificación de la derecha e izquierda.</TD>
    </TR>
    <TR>
      <TD><b><big>I4</b></TD>
      <TD>Establece relaciones entre causas y efectos: ej, cuidados que árboles y hábitat de los pájaros..</TD>
   </TR>
   
  <TR>
     <TD><b><big>I5</b></TD>
     <TD>Muestra amor y sensibilidad hacia el medio ambiente.</TD>
  </TR>
  <TR>
     <TD><b><big>I6</b></TD>
     <TD>Comprende nociones temporales: hoy, día, noche, tarde, aunque en ocasiones confunde su uso.</TD>
  </TR>
  <TR>
     <TD><b><big>I7</b></TD>
     <TD>Imita patrones con 2 figuras o métricas de 2 colores.</TD>
  </TR>
  <TR>
     <TD><b><big>I8</b></TD>
     <TD>Identifica, cuenta y comprende hasta el número 10.</TD>
  </TR>
  <TR>
     <TD><b><big>I9</b></TD>
     <TD>Mediante la manipulación de objetos cuenta, suma y resta hasta el número 10.</TD>
  </TR>
  <TR>
     <TD><b><big>I10</b></TD>
     <TD>Participa en cantos, cuentos, poemas y leyendas de su comunidad.</TD>
  </TR>
  <TR>
       <TD><b><big>I11</b></TD>
       <TD>Pasa más tiempo con su grupo de juego.</TD>
     </TR>
     <TR>
       <TD><b><big>I12</b></TD>
       <TD>Describe al menos una tradición relevante de su comunidad</TD>
    </TR>
    <TR>
     <TD><b><big>I13</b></TD>
     <TD>Juega con niñas y niños sin discriminación de género y respetando la diversidad.</TD>
  </TR>
    <TR>
      <TD><b><big>I14</b></TD>
      <TD>Practica normas de cortesía, orden, respeto y aprecio por los demás en el centro educativo, con la familia y la comunidad</TD>
   </TR>
   <TR>
   <TR>
      <TD><b><big>I15</b></TD>
      <TD>Participa con entusiasmo y creatividad en actividades lúdicas, recreativas, deportivas y otras en el centro educativo y en el hogar..</TD>
   </TR>
   <TR>     
   </TABLE>
</div>

<div class="container" style="margin-top: 10px;">
    <div class="row">
      <div class="col-md-8">
        <form action="{{ url('administracion/multiple-inserting') }}" enctype="multipart/form-data">
        {!! csrf_field() !!}



<div class="table-responsive">
  <table class="table">
  <thead>
  <th></th>
  </thead>
  <tbody>          
            @php($i=1)
            @foreach($area1 as $are1)
            @if($are1->nivel == $data2)
              <td><input type="hidden" name="hidden[]" value="{{$are1->id}}" readonly></td> 
            @php($i++)
            @endif
            @endforeach 
          </tbody>
    
  </table>
</div>


<div class="table-responsive">
  <table class="table">
  <thead>
  <tr>
              <th>Nombre</th>
              <th>Apellidos</th>
              <th>I1</th>
              <th>I2</th>
              <th>I3</th>
              <th>I4</th>
              <th>I5</th>
              <th>I6</th>
              <th>I7</th>
              <th>I8</th>
              <th>I9</th>
              <th>I10</th>
              <th>I11</th>
              <th>I12</th>
              <th>I13</th>
              <th>I14</th>
              <th>I15</th>
            </tr>
  </thead>
  <tbody>
   @php($j=1)
            @foreach($area1 as $are1)
            @if($are1->nivel == $data2)
              <td>{{$are1->record->alumnos->nombres}}</td>
              <td>{{$are1->record->alumnos->apellido_padre}}</td>
              <td ><select name="avance1[]">
              <option value="{{ $are1->I1}}">{{$are1->I1}}</option>
              <option value="S">S</option>
              <option value="T">T</option>
              <option value="P">P</option>
              </select></td>

              <td ><select name="avance2[]">
              <option value="{{ $are1->I2}}">{{$are1->I2}}</option>
              <option value="S">S</option>
              <option value="T">T</option>
              <option value="P">P</option>
              </select></td>


              <td ><select name="avance3[]">              
              <option value="{{ $are1->I3}}">{{$are1->I3}}</option>
              <option value="S">S</option>
              <option value="T">T</option>
              <option value="P">P</option>
              </select></td>

              <td ><select name="avance4[]">
              <option value="{{ $are1->I4}}">{{$are1->I4}}</option>              
              <option value="S">S</option>
              <option value="T">T</option>
              <option value="P">P</option>
              </select></td>

              <td ><select name="avance5[]">
              <option value="{{ $are1->I5}}">{{$are1->I5}}</option>              
              <option value="S">S</option>
              <option value="T">T</option>
              <option value="P">P</option>
              </select></td>

              <td ><select name="avance6[]">
              <option value="{{ $are1->I6}}">{{$are1->I6}}</option>
              <option value="S">S</option>
              <option value="T">T</option>
              <option value="P">P</option>
              </select></td>

              <td ><select name="avance7[]">
              <option value="{{ $are1->I7}}">{{$are1->I7}}</option>
              <option value="S">S</option>
              <option value="T">T</option>
              <option value="P">P</option>
              </select></td>

              <td ><select name="avance8[]">
              <option value="{{ $are1->I8}}">{{$are1->I8}}</option>
              <option value="S">S</option>
              <option value="T">T</option>
              <option value="P">P</option>
              </select></td>

              <td ><select name="avance9[]">
              <option value="{{ $are1->I9}}">{{$are1->I9}}</option>
              <option value="S">S</option>
              <option value="T">T</option>
              <option value="P">P</option>
              </select></td>

              <td ><select name="avance10[]">
              <option value="{{ $are1->I10}}">{{$are1->I10}}</option>
              <option value="S">S</option>
              <option value="T">T</option>
              <option value="P">P</option>
              </select></td>

              <td ><select name="avance11[]">
              <option value="{{ $are1->I11}}">{{$are1->I11}}</option>
              <option value="S">S</option>
              <option value="T">T</option>
              <option value="P">P</option>
              </select></td>

              <td ><select name="avance12[]">
              <option value="{{ $are1->I12}}">{{$are1->I12}}</option>
              <option value="S">S</option>
              <option value="T">T</option>
              <option value="P">P</option>
              </select></td>

              <td ><select name="avance13[]">
              <option value="{{ $are1->I13}}">{{$are1->I13}}</option>
              <option value="S">S</option>
              <option value="T">T</option>
              <option value="P">P</option>
              </select></td>

              <td ><select name="avance14[]">
              <option value="{{ $are1->I14}}">{{$are1->I14}}</option>
              <option value="S">S</option>
              <option value="T">T</option>
              <option value="P">P</option>
              </select></td>

              <td ><select name="avance15[]">
              <option value="{{ $are1->I15}}">{{$are1->I15}}</option>
              <option value="S">S</option>
              <option value="T">T</option>
              <option value="P">P</option>
              </select></td>           

            </tr>
            @php($j++)
            @endif
            @endforeach
    
  </tbody>
    
  </table>
  <td><input type="hidden" name="identificador" value="{{$data3}}" readonly></td> 
</div> 
<hr>

        <div class="form-group">
          <button class="btn btn-primary btn-sm">
            Ingresar
          </button>          
        </div>
        </form>
      </div>
    </div>
  </div>

  @endsection