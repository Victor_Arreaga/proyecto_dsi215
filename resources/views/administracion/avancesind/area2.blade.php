@extends('templates.main')

@section('title', 'Nomina de Alumnos de Parvularia')

@section('content')
<h4 align="center">Prof: {{$data0}} {{$data1}}</h4>
<h4 align="center">Indicadores de Logro por Areas de {{$data3}}</h4>
<h4 align="center">{{$data2}}         {{$year}}</h4>
<hr>
<div align="CENTER">
    <TABLE BORDER=2 bordercolor="red" align="CENTER">
     <TR>
       <TD><b><big>I1</b></TD>
       <TD>Dibuja figura humana proporcionada con cabeza, tronco, extremidades.</TD>
      </TR>
     <TR>
        <TD><b><big>I2</b></TD>
        <TD>Juega a disfrazarse imitando espontáneamente roles de su preferencia.</TD>
     </TR>
     <TR>
       <TD><b><big>I3</b></TD>
       <TD>Identifica y nombra el rectángulo y el rombo</TD>
    </TR>
    <TR>
      <TD><b><big>I4</b></TD>
      <TD>Tararea y canta canciones.</TD>
   </TR>
   <TR>
     <TD><b><big>I5</b></TD>
     <TD>Comprende y puede atender hasta más de tres indicaciones relacionadas.</TD>
  </TR>
  <TR>
     <TD><b><big>I6</b></TD>
     <TD>Comprende un vocabulario de 1500 palabras.</TD>
  </TR>
  <TR>
     <TD><b><big>I7</b></TD>
     <TD>Elabora y responde preguntas.</TD>
  </TR>
  <TR>
     <TD><b><big>I8</b></TD>
     <TD>Mantiene una conversación con un adulto y la acompaña de gestos.</TD>
  </TR>
  <TR>
     <TD><b><big>I9</b></TD>
     <TD>Relata experiencias del acontecer diario empleando ayer y mañana.</TD>
  </TR>
  <TR>
     <TD><b><big>I10</b></TD>
     <TD>Relata cuentos cortos utilizando enlaces: luego, después etc. y sus ideas tienen un inicio, desarrollo y final.</TD>
  </TR>
  <TR>
     <TD><b><big>I11</b></TD>
     <TD>Demuestra su sentido del humor expresando chistes, rimas graciosas canciones o adivinanzas.</TD>
  </TR>
  <TR>
       <TD><b><big>I12</b></TD>
       <TD>Usa el color a veces y relacionarlo con la realidad.</TD>
     </TR>
     <TR>
       <TD><b><big>I13</b></TD>
       <TD>Crea historias con secuencias de tiempo (que sucedió primero, segundo, después etc.)va</TD>
    </TR>
    <TR>
      <TD><b><big>I14</b></TD>
      <TD>Usa lenguaje oral para expresarse y comunicar sus ideas, sus dudas y sentimientos.</TD>
   </TR>
   <TR>
   <TR>
      <TD><b><big>I15</b></TD>
      <TD>Participa en actividades culturales manuales gráfico-plásticas musicales y lúdico-creativas.</TD>
   </TR>
   <TR>     
   </TABLE>
</div>

<div class="container" style="margin-top: 10px;">
    <div class="row">
      <div class="col-md-8">
        <form action="{{ url('administracion/multiple-inserting') }}" enctype="multipart/form-data">
        {!! csrf_field() !!}



<div class="table-responsive">
  <table class="table">
  <thead>
  <th></th>
  </thead>
  <tbody>          
            @php($i=1)
            @foreach($area1 as $are1)
            @if($are1->nivel == $data2)
              <td><input type="hidden" name="hidden[]" value="{{$are1->id}}" readonly></td> 
            @php($i++)
            @endif
            @endforeach 
          </tbody>
    
  </table>
</div>


<div class="table-responsive">
  <table class="table">
  <thead>
  <tr>
              <th>Nombre</th>
              <th>Apellidos</th>
              <th>I1</th>
              <th>I2</th>
              <th>I3</th>
              <th>I4</th>
              <th>I5</th>
              <th>I6</th>
              <th>I7</th>
              <th>I8</th>
              <th>I9</th>
              <th>I10</th>
              <th>I11</th>
              <th>I12</th>
              <th>I13</th>
              <th>I14</th>
              <th>I15</th>
            </tr>
  </thead>
  <tbody>
   @php($j=1)
            @foreach($area1 as $are1)
            @if($are1->nivel == $data2)
              <td>{{$are1->record->alumnos->nombres}}</td>
              <td>{{$are1->record->alumnos->apellido_padre}}</td>
              <td ><select name="avance1[]">
              <option value="{{ $are1->I1}}">{{$are1->I1}}</option>
              <option value="S">S</option>
              <option value="T">T</option>
              <option value="P">P</option>
              </select></td>

              <td ><select name="avance2[]">
              <option value="{{ $are1->I2}}">{{$are1->I2}}</option>
              <option value="S">S</option>
              <option value="T">T</option>
              <option value="P">P</option>
              </select></td>


              <td ><select name="avance3[]">              
              <option value="{{ $are1->I3}}">{{$are1->I3}}</option>
              <option value="S">S</option>
              <option value="T">T</option>
              <option value="P">P</option>
              </select></td>

              <td ><select name="avance4[]">
              <option value="{{ $are1->I4}}">{{$are1->I4}}</option>              
              <option value="S">S</option>
              <option value="T">T</option>
              <option value="P">P</option>
              </select></td>

              <td ><select name="avance5[]">
              <option value="{{ $are1->I5}}">{{$are1->I5}}</option>              
              <option value="S">S</option>
              <option value="T">T</option>
              <option value="P">P</option>
              </select></td>

              <td ><select name="avance6[]">
              <option value="{{ $are1->I6}}">{{$are1->I6}}</option>
              <option value="S">S</option>
              <option value="T">T</option>
              <option value="P">P</option>
              </select></td>

              <td ><select name="avance7[]">
              <option value="{{ $are1->I7}}">{{$are1->I7}}</option>
              <option value="S">S</option>
              <option value="T">T</option>
              <option value="P">P</option>
              </select></td>

              <td ><select name="avance8[]">
              <option value="{{ $are1->I8}}">{{$are1->I8}}</option>
              <option value="S">S</option>
              <option value="T">T</option>
              <option value="P">P</option>
              </select></td>

              <td ><select name="avance9[]">
              <option value="{{ $are1->I9}}">{{$are1->I9}}</option>
              <option value="S">S</option>
              <option value="T">T</option>
              <option value="P">P</option>
              </select></td>

              <td ><select name="avance10[]">
              <option value="{{ $are1->I10}}">{{$are1->I10}}</option>
              <option value="S">S</option>
              <option value="T">T</option>
              <option value="P">P</option>
              </select></td>

              <td ><select name="avance11[]">
              <option value="{{ $are1->I11}}">{{$are1->I11}}</option>
              <option value="S">S</option>
              <option value="T">T</option>
              <option value="P">P</option>
              </select></td>

              <td ><select name="avance12[]">
              <option value="{{ $are1->I12}}">{{$are1->I12}}</option>
              <option value="S">S</option>
              <option value="T">T</option>
              <option value="P">P</option>
              </select></td>

              <td ><select name="avance13[]">
              <option value="{{ $are1->I13}}">{{$are1->I13}}</option>
              <option value="S">S</option>
              <option value="T">T</option>
              <option value="P">P</option>
              </select></td>

              <td ><select name="avance14[]">
              <option value="{{ $are1->I14}}">{{$are1->I14}}</option>
              <option value="S">S</option>
              <option value="T">T</option>
              <option value="P">P</option>
              </select></td>

              <td ><select name="avance15[]">
              <option value="{{ $are1->I15}}">{{$are1->I15}}</option>
              <option value="S">S</option>
              <option value="T">T</option>
              <option value="P">P</option>
              </select></td>           

            </tr>
            @php($j++)
            @endif
            @endforeach
    
  </tbody>
    
  </table>
  <td><input type="hidden" name="identificador" value="{{$data3}}" readonly></td> 
</div> 
<hr>

        <div class="form-group">
          <button class="btn btn-primary btn-sm">
            Ingresar
          </button>          
        </div>
        </form>
      </div>
    </div>
  </div>

  @endsection