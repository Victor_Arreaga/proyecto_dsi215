@extends('templates.main')

@section('title', 'Registro Alumno')

@section('content')
	{!! Form::open(['route'=>'alumnos.store' , 'method'=>'POST']) !!}

	<div class="form-group">
		{!! Form::label('nombres', 'Nombres del Alumno') !!}

		{!! Form::text('nombres', null, ['class'=>'form-control', 'placeholder'=>'Nombres del Alumno', 'required',]) !!}
	</div>

	<div class="form-group">
		{!! Form::label('apellido_padre', 'Paterno') !!}

		{!! Form::text('apellido_padre', null, ['class'=>'form-control', 'placeholder'=>'Apellido Paterno', 'required']) !!}
	</div>

	<div class="form-group">
		{!! Form::label('apellido_madre', 'Apellido Materno') !!}

		{!! Form::text('apellido_madre', null, ['class'=>'form-control', 'placeholder'=>'Apellido Materno', 'required']) !!}
	</div>

	<div class="form-group">
		{!! Form::label('sexo', 'Sexo') !!}

		{!! Form::select('sexo', [''=>'Selecione','Femenino'=>'Femenino', 'Masculino'=>'Masculino'], null, ['class'=>'form-control']) !!}
	</div>

    <div class="form-group">
		{!! Form::label('fecha_nacimiento', 'Fecha de Nacimiento') !!}

		{!! Form::text('fecha_nacimiento', null, ['class'=>'form-control', 'id'=>'datepicker','readonly'=>'true', 'placeholder'=>'AAAA/MM/DD', 'required']) !!}
	</div>

	<div class="form-group">
		{!! Form::label('repite', 'Repite Grado') !!}

		{!! Form::select('repite', [''=>'Selecione','Si'=>'Si', 'No'=>'No'], null, ['class'=>'form-control']) !!}
	</div>

	<div class="form-group">
		{!! Form::label('estudio_parvularia', 'Estudio Parvularia') !!}

		{!! Form::select('estudio_parvularia', [''=>'Selecione','Si'=>'Si', 'No'=>'No'], null, ['class'=>'form-control']) !!}
	</div>

	<div class="form-group">
		{!! Form::label('zona_residencia', 'Zona de Residencia') !!}

		{!! Form::select('zona_residencia', [''=>'Selecione','Rural'=>'Rural', 'Urbana'=>'Urbana'], null, ['class'=>'form-control']) !!}
	</div>

    <div class="form-group">
		{!! Form::label('cod_depto_residencia', 'Codigo Departamento de Residencia') !!}

		{!! Form::text('cod_depto_residencia', null, ['class'=>'form-control', 'placeholder'=>'####', 'required']) !!}
	</div>

    <div class="form-group">
		{!! Form::label('actividad_econ', 'Actividad Economica') !!}

		{!! Form::select('actividad_econ', [''=>'Selecione','Si'=>'Si', 'No'=>'No'], null, ['class'=>'form-control']) !!}
	</div>

    <div class="form-group">
		{!! Form::label('tipos_discapacidad', 'Tipos de Discapacidad') !!}

		{!! Form::text('tipos_discapacidad', null, ['class'=>'form-control', 'placeholder'=>'Tipos Discapacidad', 'required']) !!}
	</div>

	 <div class="form-group">
		{!! Form::label('encargado', 'Persona Encargada') !!}

		{!! Form::text('encargado', null, ['class'=>'form-control', 'placeholder'=>'Nombre Persona Encargada', 'required']) !!}
	</div>

	 <div class="form-group">
		{!! Form::label('direccion', 'Direccion') !!}

		{!! Form::text('direccion', null, ['class'=>'form-control', 'placeholder'=>'Direccion', 'required']) !!}
	</div>

	

         <div class="form-group">
		{!! Form::submit('Registrar', ['class'=>'btn btn-primary']) !!}
		 <!--<a href="{{ route('alumnos.index') }}" class="btn btn-info">Cancelar</a><hr>-->
     	</div>

	{!! Form::close() !!}
@endsection
