@extends('templates.main')

@section('title', 'Registro de Notas por Materia')

@section('content')

    <h3 align="center">Notas de {{$identificador->nombre}}</h3>
    <h3 align="center">Matemáticas</h3>


    {!! Form::open(['route'=>['notas.update', $identificador], 'method'=>'PUT']) !!}

    <table class="table table-condensed">

        <thead>
          <th>Nombres</th>
          <th>Apellidos</th>
          <th></th>
          <th>Trimestre</th>
          <th>nota 1</th>
          <th>nota 2</th>
          <th>nota 3</th>
          <th></th>

        </thead>

        <tbody>


            @foreach($registros as $registro)
               @if($registro->idgrado == $identificador->id)
							  @if ($registro->activo == 1)
                   <tr>
                     <td>{{ $registro->alumnos->nombres }}</td>
                     <td>{{ $registro->alumnos->apellido_padre }} {{ $registro->alumnos->apellido_madre }}</td>
  {!! Form::close() !!}

{!! Form::open(['route'=>'notas.store', 'method'=>'POST']) !!}
<td>
<div class="form-group">
  {!!  Form::hidden('idrecord', $registro->id)!!}
</div>
</td>
<td style="width:50px;">
  <div class="form-group">
      {!! Form::select('tipo', [''=>'Trim','1Trimestre'=>'1Trimestre', '2Trimestre'=>'2Trimestre', '3Trimestre'=>'3Trimestre'], null, ['class'=>'form-control']) !!}
  </div>
</td>
<td style="width:85px;">
<div class="form-group">
 {!! Form::text('nota1', null, ['class'=>'form-control', 'placeholder'=>'00.00']) !!}
</div>
</td>
<td style="width:85px;">
<div class="form-group">
 {!! Form::text('nota2', null, ['class'=>'form-control', 'placeholder'=>'00.00']) !!}
</div>
</td>
<td style="width:85px;">
<div class="form-group">
 {!! Form::text('nota3', null, ['class'=>'form-control', 'placeholder'=>'00.00']) !!}
</div>
</td>
<td>
<div class="form-group">
 {!! Form::hidden('promedio', '(nota1+nota2+nota3)/3') !!}
</div>
</td>
<td>
<div class="form-group">
  {!!  Form::hidden('idasignatura', '1')!!}
</div>
</td>
                      <td><div class="form-group">
                    		{!! Form::submit('Registrar', ['class'=>'btn btn-primary']) !!}
                    	</div></td>
{!! Form::close() !!}
                  </tr>

								@endif
             @endif
             @endforeach

        </tbody>
	</table>




@endsection
