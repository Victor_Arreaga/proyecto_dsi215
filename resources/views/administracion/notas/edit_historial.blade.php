@extends('templates.main')

@section('title', 'Historial de Notas del Alumno')

@section('content')

    <h3 align="center">{{$identificador->nombre}}</h3>
    <h3 align="center">{{$alumno->nombres}} {{$alumno->apellido_padre}} {{$alumno->apellido_madre}}</h3>
    <h3 align="center">Primer Trimestre</h3>

    {!! Form::open(['route'=>['notas.update', $identificador], 'method'=>'PUT']) !!}
<!--Registro de Notas Primer Trimestre -->
    <table class="table table-condensed">
        <thead>
          <th>Materias</th>
          <th>Nota 1</th>
          <th>Nota 2</th>
          <th>Nota 3</th>
          <th>Promedio</th>
        </thead>

        <tbody>
            @foreach($atribuciones as $atribucion)
               @if($atribucion->idgrado == $identificador->id)
                <tr>
                  <td>{{ $atribucion->asignaturas->nombre }}</td>
                       @foreach($notas as $nota)
                           @if($nota->records->idgrado == $identificador->id)
                               @if($nota->records->idalumno == $alumno->id)
                                  @if($nota->idasignatura == $atribucion->idasignatura)
                                     @if($nota->idtrimestre == '1')
                                        <td>{{ $nota->nota1 }}</td>
                                        <td>{{ $nota->nota2 }}</td>
                                        <td>{{ $nota->nota3 }}</td>
                                        <td>{{ $nota->promedio }}</td>
                                     @endif
                                  @endif
                               @endif
                           @endif
                       @endforeach
                </tr>
             @endif
             @endforeach
        </tbody>
	</table>
<!--Conducta -->
  <table align="center" WIDTH="55%" heigth="70%" border="5px">
      <thead>
        <th>Conceptos</th>
        <th>Conducta</th>
      </thead>

      <tbody>

          @foreach($conductas as $conducta)
             @if($conducta->records->idgrado == $identificador->id)
               @if($conducta->records->idalumno == $alumno->id)
                 @if($conducta->idtrimestre == '1')
                  <tr>
                    <TR>
                      <TD>Se respeta a sí mismo (a) y a los demás.</TD>
                      <td>{{ $conducta->c1 }}</td>
         	          </TR>
         	          <TR>
                      <TD>Convive de forma armónica y solidaria.</TD>
                      <td>{{ $conducta->c2 }}</td>
         	         </TR>
                   <TR>
                     <TD>Toma decisiones responsablemente.</TD>
                     <td>{{ $conducta->c3 }}</td>
                  </TR>
                  <TR>
                    <TD>Cumple los deberes y ejerce correctamente sus derechos.</TD>
                    <td>{{ $conducta->c4 }}</td>
                  </TR>
                  <TR>
                    <TD>Practica valores morales y cívicos.</TD>
                    <td>{{ $conducta->c5 }}</td>
                  </TR>
                @endif
              @endif

              </tr>
           @endif
           @endforeach
      </tbody>
</table>

<!--Registro de Notas Segundo Trimestre -->
</hr>
  <h3 align="center">Segundo Trimestre</h3>
  <table class="table table-condensed">
      <thead>
        <th>Materias</th>
        <th>Nota 1</th>
        <th>Nota 2</th>
        <th>Nota 3</th>
        <th>Promedio</th>
      </thead>
      <tbody>
          @foreach($atribuciones as $atribucion)
             @if($atribucion->idgrado == $identificador->id)
              <tr>
                <td>{{ $atribucion->asignaturas->nombre }}</td>
                     @foreach($notas as $nota)
                         @if($nota->records->idgrado == $identificador->id)
                             @if($nota->records->idalumno == $alumno->id)
                                @if($nota->idasignatura == $atribucion->idasignatura)
                                   @if($nota->idtrimestre == '2')
                                      <td>{{ $nota->nota1 }}</td>
                                      <td>{{ $nota->nota2 }}</td>
                                      <td>{{ $nota->nota3 }}</td>
                                      <td>{{ $nota->promedio }}</td>
                                   @endif
                                @endif
                             @endif
                         @endif
                     @endforeach
              </tr>
           @endif
           @endforeach
      </tbody>
</table>
<!--Conducta -->
  <table align="center" WIDTH="55%" heigth="70%" border="5px">
      <thead>
        <th>Conceptos</th>
        <th>Conducta</th>
      </thead>

      <tbody>

          @foreach($conductas as $conducta)
             @if($conducta->records->idgrado == $identificador->id)
               @if($conducta->records->idalumno == $alumno->id)
                 @if($conducta->idtrimestre == '2')
                  <tr>
                    <TR>
                      <TD>Se respeta a sí mismo (a) y a los demás.</TD>
                      <td>{{ $conducta->c1 }}</td>
         	          </TR>
         	          <TR>
                      <TD>Convive de forma armónica y solidaria.</TD>
                      <td>{{ $conducta->c2 }}</td>
         	         </TR>
                   <TR>
                     <TD>Toma decisiones responsablemente.</TD>
                     <td>{{ $conducta->c3 }}</td>
                  </TR>
                  <TR>
                    <TD>Cumple los deberes y ejerce correctamente sus derechos.</TD>
                    <td>{{ $conducta->c4 }}</td>
                  </TR>
                  <TR>
                    <TD>Practica valores morales y cívicos.</TD>
                    <td>{{ $conducta->c5 }}</td>
                  </TR>
                @endif
              @endif

              </tr>
           @endif
           @endforeach
      </tbody>
</table>

<!--Registro de Notas Tercer Trimestre -->
</hr>
<h3 align="center">Tercer Trimestre</h3>
<table class="table table-condensed">
    <thead>
      <th>Materias</th>
      <th>Nota 1</th>
      <th>Nota 2</th>
      <th>Nota 3</th>
      <th>Promedio</th>
    </thead>
    <tbody>
        @foreach($atribuciones as $atribucion)
           @if($atribucion->idgrado == $identificador->id)
            <tr>
              <td>{{ $atribucion->asignaturas->nombre }}</td>
                   @foreach($notas as $nota)
                       @if($nota->records->idgrado == $identificador->id)
                           @if($nota->records->idalumno == $alumno->id)
                              @if($nota->idasignatura == $atribucion->idasignatura)
                                 @if($nota->idtrimestre == '3')
                                    <td>{{ $nota->nota1 }}</td>
                                    <td>{{ $nota->nota2 }}</td>
                                    <td>{{ $nota->nota3 }}</td>
                                    <td>{{ $nota->promedio }}</td>
                                 @endif
                              @endif
                           @endif
                       @endif
                   @endforeach
            </tr>
         @endif
         @endforeach
    </tbody>
</table>
<!--Conducta -->
  <table align="center" WIDTH="55%" heigth="70%" border="5px">
      <thead>
        <th>Conceptos</th>
        <th>Conducta</th>
      </thead>

      <tbody>

          @foreach($conductas as $conducta)
             @if($conducta->records->idgrado == $identificador->id)
               @if($conducta->records->idalumno == $alumno->id)
                 @if($conducta->idtrimestre == '3')
                  <tr>
                    <TR>
                      <TD>Se respeta a sí mismo (a) y a los demás.</TD>
                      <td>{{ $conducta->c1 }}</td>
         	          </TR>
         	          <TR>
                      <TD>Convive de forma armónica y solidaria.</TD>
                      <td>{{ $conducta->c2 }}</td>
         	         </TR>
                   <TR>
                     <TD>Toma decisiones responsablemente.</TD>
                     <td>{{ $conducta->c3 }}</td>
                  </TR>
                  <TR>
                    <TD>Cumple los deberes y ejerce correctamente sus derechos.</TD>
                    <td>{{ $conducta->c4 }}</td>
                  </TR>
                  <TR>
                    <TD>Practica valores morales y cívicos.</TD>
                    <td>{{ $conducta->c5 }}</td>
                  </TR>
                @endif
              @endif

              </tr>
           @endif
           @endforeach
      </tbody>
</table>

<!--Registro de Notas Finales -->

</table>
@endsection
