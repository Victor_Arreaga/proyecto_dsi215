<!--
-Archivo main.blade.php
-Contiene una plantilla principal HTML que será usada por las vistas
-Estas vistas heredan este código para un funcionamiento uniforme de los estilos y
 scripts
-->
<!DOCTYPE html>

<html lang="es">
  <head>
    <meta charset="utf-8">
    <title> @yield('title', 'default') | Panel de Administración</title>
    <link rel="stylesheet" href="{{ asset('bootstrap/css/bootstrap.css')}}">
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
   
  </head>

  <body class="admin-body col-md-8 col-lg-offset-2" onload="estiloTabla()">
    @include('templates.partials.nav')
        <section class="section-admin">
        <div class="panel panel-default">
          <div class="panel-heading">
          <br><br><br>
          <h3 class="panel-title">@yield('title')</h3>
          </div>
          <div class="panel-body">
            @include('flash::message')
            @include('templates.partials.errors')
            @yield('content')
          </div>
        </div>
    </section>
    <!--Scripts para las funcionalidades de las vistas-->
   

    <script src="{{ asset('jquery/js/jquery-1.12.4.js')}}"></script>
    <script src="https://code.jquery.com/ui/1.11.3/jquery-ui.js"></script>
    <script src="{{ asset('bootstrap/js/bootstrap.js')}}"></script>
    <script src="{{ asset('js/tablas.js')}}"></script>

    


        
        
        <link rel="stylesheet" href="/bower_components/bootstrap/datatables/css/dataTables.bootstrap.min.css">

        <script src="{{ asset('bower_components/bootstrap/datatables/js/jquery.dataTables.min.js') }}" type="text/javascript"></script>  <!--Aca esta la config de buscar pasado por kirio-->

        <script src="{{ asset('bower_components/bootstrap/datatables/js/dataTables.bootstrap.min.js') }}" type="text/javascript"></script>
            

         <link rel="stylesheet" href="/bower_components/bootstrap/datatables/css/jquery.dataTables.min.css">

        <script src="{{ asset('bower_components/bootstrap/datatables-responsive/dataTables.responsive.js') }}" type="text/javascript"></script>
        



      
  


<!--
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

-->


    <!--Script para el uso de calendario en al insertar fechas-->
    <script type="text/javascript">
    jQuery(function($){
      $.datepicker.regional['es'] = {
        closeText: 'Cerrar',
        prevText: '&#x3c;Ant',
        nextText: 'Sig&#x3e;',
        currentText: 'Hoy',
        monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
        monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'],
        dayNames: ['Domingo','Lunes','Martes','Mi&eacute;rcoles','Jueves','Viernes','S&aacute;bado'],
        dayNamesShort: ['Dom','Lun','Mar','Mi&eacute;','Juv','Vie','S&aacute;b'],
        dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','S&aacute;'],
        weekHeader: 'Sm',
        dateFormat: "yy-mm-dd",
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''};
      $.datepicker.setDefaults($.datepicker.regional['es']);});
    $(function() {
      $("#datepicker").datepicker({changeYear: true }).val()
    });
    </script>



    <script language="javascript">
    function doSearch()
    {
      var tableReg = document.getElementById('datos');
      var searchText = document.getElementById('searchTerm').value.toLowerCase();
      var cellsOfRow="";
      var found=false;
      var compareWith="";
 
      // Recorremos todas las filas con contenido de la tabla
      for (var i = 1; i < tableReg.rows.length; i++)
      {
        cellsOfRow = tableReg.rows[i].getElementsByTagName('td');
        found = false;
        // Recorremos todas las celdas
        for (var j = 0; j < cellsOfRow.length && !found; j++)
        {
          compareWith = cellsOfRow[j].innerHTML.toLowerCase();
          // Buscamos el texto en el contenido de la celda
          if (searchText.length == 0 || (compareWith.indexOf(searchText) > -1))
          {
            found = true;
          }
        }
        if(found)
        {
          tableReg.rows[i].style.display = '';
        } else {
          // si no ha encontrado ninguna coincidencia, esconde la
          // fila de la tabla
          tableReg.rows[i].style.display = 'none';
        }
      }
    }
  </script>


  </body>
</html>
