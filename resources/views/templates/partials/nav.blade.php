
<nav class="navbar navbar-inverse navbar-fixed-top">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand">CECLI</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

    @if(Auth::user()->rol=='Director')  <!-- IF PARA ENTRAR A PANEL DE DIRECTOR -->
      <ul class="nav navbar-nav">
        <li class="active"><a href="{{ route('home') }}">Inicio<span class="sr-only">(current)</span></a></li>

        <ul class="nav navbar-nav navbar-right">
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
            Asignaciones<span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li>
             <a href="{{ route('asignaciones.index') }}">Asignacion Docentes a Grado</a></li>
              </li>
              <li>
               <a href="{{ route('atribuciones.index') }}">Asignacion Asignaturas a Grado</a></li>
                </li>
          </ul>
        </li>
      </ul>

        <ul class="nav navbar-nav navbar-right">
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Administracion<span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li>
              <a href="{{ route('usuarios.index') }}">Gestión de Usuarios</a>
            </li>
            <li>
              <a href="{{ route('docentes.index') }}">Gestión de Docentes</a>
            </li>
            <li>
              <a href="{{ route('grados.index') }}">Gestión de Grados</a></li>
            </li>
            <li>
              <li><a href="{{ route('asignaturas.index') }}">Gestión de Asignaturas</a></li>
            </li>
            <li>
              <li><a href="{{ route('alumnos.index') }}">Gestión de Alumnos</a></li>
            </li>
          </ul>
        </li>
      </ul>




      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{{ Auth::user()->email }}<span class="caret"></span></a>

          <ul class="dropdown-menu">

            <li>
              <a href="{{url('usuarios/password')}}">Cambiar mi password</a>

            </li>

            <li>
              <a href="{{ route('logout') }}"
                  onclick="event.preventDefault();
                  document.getElementById('logout-form').submit();">
                  Salir</a>
                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}</form>
              </li>


          </ul>
        </li>
      </ul>

      @elseif (Auth::user()->rol=='Docente Parvularia')

      <ul class="nav navbar-nav">
        <li class="active"><a href="{{ route('home') }}">Inicio<span class="sr-only">(current)</span></a></li>
        <ul class="nav navbar-nav navbar-right">
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Administracion de Parvularia<span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="{{ route('area_indicadores.index') }}">Gestión de Area</a></li>
            <li><a href="{{ route('indicadors.index') }}">Gestion de Indicadores</a></li>
            <li><a href="{{ route('indicador_assignment.index')}}">Registro de Indicadores</a></li>

            </ul>
        </li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
      <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Administracion<span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li>
            <li><a href="{{ route('otros.index',[Auth::user()->iddocente])}}">Gestión de Alumnos</a></li>
          </li>
        </ul>
      </li>
    </ul>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> {{ Auth::user()->email}} <span class="caret"></span></a>

          <ul class="dropdown-menu">

            <li>
              <a href="{{url('usuarios/password')}}">Cambiar mi password</a>

            </li>
            <li>
              <a href="{{ route('logout') }}"
                  onclick="event.preventDefault();
                  document.getElementById('logout-form').submit();">
                  Salir</a>
                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}</form>
              </li>
            </ul>
        </li>
      </ul>


      @else <!-- ELSE PARA ENTRAR A PANEL DE DOCENTE -->
      <ul class="nav navbar-nav">
        <li class="active"><a href="{{ route('home') }}">Inicio<span class="sr-only">(current)</span></a></li>
      <ul class="nav navbar-nav navbar-right">
      <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Administracion<span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li>
            <li><a href="{{ route('otros.index') }}">Gestión de Alumnos</a></li>
            <li><a href="{{ route('otros.create') }}">Registro de Notas</a></li>
            <li><a href="{{ route('asistencias.index') }}">Registro de Asistencia</a></li>
            <li><a href="#">Reportes</a></li>

          </li>
        </ul>
      </li>
    </ul>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> {{ Auth::user()->email}} <span class="caret"></span></a>

          <ul class="dropdown-menu">

            <li>
              <a href="{{url('usuarios/password')}}">Cambiar mi password</a>

            </li>
            <li>
              <a href="{{ route('logout') }}"
                  onclick="event.preventDefault();
                  document.getElementById('logout-form').submit();">
                  Salir</a>
                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}</form>
              </li>
            </ul>
        </li>
      </ul>
      @endif
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
