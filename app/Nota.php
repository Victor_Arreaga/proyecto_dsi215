<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Nota extends Model
{
    protected $fillable=[ 'idrecord','idtrimestre', 'nota1', 'nota2', 'nota3', 'promedio', 'idasignatura'];

    public function records(){
        return $this->belongsTo('App\Record', 'idrecord');
    }

    public function asignaturas(){
        return $this->belongsTo('App\Asignatura', 'idasignatura');
    }

    public function trimestres(){
        return $this->belongsTo('App\Trimestre', 'idtrimestre');
    }

    public function conductas(){
        return $this->belongsTo('App\Conducta', 'idconducta');
    }

    public function grados(){
        return $this->belongsTo('App\Asignatura', 'idgrado');
    }
}
