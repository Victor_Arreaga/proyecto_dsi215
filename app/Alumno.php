<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Alumno extends Model
{
  protected $fillable = [

            'nombres',
            'apellido_padre',
            'apellido_madre',
            'sexo',
            'fecha_nacimiento',
            'repite',
            'estudio_parvularia',
            'zona_residencia',
            'cod_depto_residencia',
            'actividad_econ',
            'tipos_discapacidad',
            'encargado',
            'direccion'


  ];

   public function records(){
        return $this->belongsTo('App\Record');
    }

     public function asistencia(){
        return $this->belongsTo('App\Asistencia');
    }

}
