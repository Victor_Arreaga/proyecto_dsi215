<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Alumno;
use App\Record;
use App\Grado;
use App\Attribution;
use App\Asignatura;
use Session;
use Auth;
use App\Assignment;
use App\Trimestre;


class OtrosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      //$registros = Record::with('alumnos', 'grados')->orderBy('id', 'ASC')->paginate(20);

      //$registros =Record::search($request->idgrado)->orderBy('id', 'ASC')->paginate(10);
      //$idgrado="Cuarto A";

      $trimestres = Trimestre ::orderBy('id', 'ASC')->pluck('nombre', 'id');
      $grados = Asignatura ::orderBy('id', 'ASC')->pluck('nombre', 'id');
      $atribuciones = Attribution :: with('grados', 'asignaturas')->orderBy('id', 'ASC')->paginate(20);
      $registros = Assignment::with('docentes', 'grados')->orderBy('id', 'ASC')->paginate(20);

      return view('administracion.otros.index')->with('registros', $registros)->with('registros', $registros)->with('atribuciones', $atribuciones)->with('trimestres', $trimestres)->with('grados', $grados);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      //$registros = Record::with('alumnos', 'grados')->orderBy('id', 'ASC')->paginate(20);

      //$registros =Record::search($request->idgrado)->orderBy('id', 'ASC')->paginate(10);
      //$idgrado="Cuarto A";

      $trimestres = Trimestre ::orderBy('id', 'ASC')->pluck('nombre', 'id');
      $grados = Asignatura ::orderBy('id', 'ASC')->pluck('nombre', 'id');
      $atribuciones = Attribution :: with('grados', 'asignaturas')->orderBy('id', 'ASC')->paginate(20);
      $registros = Assignment::with('docentes', 'grados')->orderBy('id', 'ASC')->paginate(20);

      return view('administracion.otros.create')->with('registros', $registros)->with('registros', $registros)->with('atribuciones', $atribuciones)->with('trimestres', $trimestres)->with('grados', $grados);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($idgrado)
    {
      $identificador = Grado::find($idgrado);
      //$ident = Asignatura::find($idasignatura);

    //  $asignaciones = Assignment :: with('docentes', 'grados')->orderBy('id', 'ASC')->paginate(20);
      $atribuciones = Attribution :: with('grados', 'asignaturas')->orderBy('id', 'ASC')->paginate(20);
      $registros = Record :: with('alumnos', 'grados')->orderBy('id', 'ASC')->paginate(20);
      $alumnos= Alumno::orderBy('id', 'ASC')->pluck('nombres', 'id');

      return view('administracion.otros.show')->with('identificador', $identificador)->with('registros', $registros)->with('alumnos', $alumnos)->with('atribuciones', $atribuciones);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function edit($idgrado)
     {
       $identificador = Grado::find($idgrado);
       //$ident = Asignatura::find($idasignatura);

     //  $asignaciones = Assignment :: with('docentes', 'grados')->orderBy('id', 'ASC')->paginate(20);
       $atribuciones = Attribution :: with('grados', 'asignaturas')->orderBy('id', 'ASC')->paginate(20);
       $registros = Record :: with('alumnos', 'grados')->orderBy('id', 'ASC')->paginate(20);
       $alumnos= Alumno::orderBy('id', 'ASC')->pluck('nombres', 'id');

       return view('administracion.otros.edit')->with('identificador', $identificador)->with('registros', $registros)->with('alumnos', $alumnos)->with('atribuciones', $atribuciones);
     }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $identificador=Grado::find($id);
    //  $ident=Alumno::find($id);

      $registros=new Record($request->all());
      $registros->fill($request->all());
    //  $registros->$idalumno->asignado=1;
      $registros->save();

      flash()->warning('La asignacion ha sido almacenada con exito');

      return redirect()->route('otros.edit', $identificador);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      //$identificador=$id;
      $registros = Record::find($id);
      $registros->activo=0;
      $registros->save();

      flash()->warning('El registro '.$registros->id.' ha sido dado de baja');

      return redirect()->route('otros.edit', $registros->idgrado);
    }

}
