<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AreaIndicador;

class BareaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        {
        $area_indicadores= AreaIndicador::orderBy('id', 'ASC')->paginate(250);
      return view('administracion.areaindicadores.bajaindex')->with('area_indicadores', $area_indicadores);
    }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       $area_indicadores=AreaIndicador::find($id);
        return view('administracion.areaindicadores.alta')->with('area_indicadores', $area_indicadores);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       
        $area_indicadores=AreaIndicador::find($id);
        $area_indicadores->fill($request->all());
            $area_indicadores->save();
     flash()->warning('El Registro del Area '. $area_indicadores->nombre. ' ha sido dado de alta');

        return redirect()->route('area_indicadores.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
