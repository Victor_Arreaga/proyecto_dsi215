<?php

namespace App\Http\Controllers;
use App\Asignatura;

use Illuminate\Http\Request;
use App\Http\Requests\AsignaturaRequest;


class AsignaturasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $asignaturas= Asignatura::orderBy('id', 'ASC')->paginate(250);

        return view('administracion.asignaturas.index')->with('asignaturas', $asignaturas);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('administracion.asignaturas.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AsignaturaRequest $request)
    {
        $asignatura=new Asignatura($request->all());
        $asignatura->save();
        flash()->success('Se ha registrado la asignatura de forma exitosa');

        return redirect()->route('asignaturas.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $asignatura=Asignatura::find($id);
        return view('administracion.asignaturas.edit')->with('asignatura', $asignatura);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AsignaturaRequest $request, $id)
    {
     $asignatura=Asignatura::find($id);
        $asignatura->fill($request->all());
        $asignatura->save();

        flash()->success('Los datos han sido actualizado con exito');

        return redirect()->route('asignaturas.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function destroy($id)
    {
        $asignatura = Asignatura::find($id);
        $asignatura->activo=0;
        $asignatura->save();
        flash()->success('La asignatura '.$asignatura->nombre.' ha sido dada de baja');
        return redirect()->route('asignaturas.index');
    }

    public function bajasasigindex()
    {
        $asignaturas= Asignatura::orderBy('id', 'ASC')->paginate(250);

        return view('administracion.asignaturas.bajas')->with('asignaturas', $asignaturas);
    }

    public function altaasig($id)
    {
        $asignatura = Asignatura::find($id);
        $asignatura->activo=1;
        $asignatura->save();
        flash()->success('La asignatura '.$asignatura->nombre.' ha sido dada de alta');
        return redirect()->route('asignaturas.index');
          
    }
}
