<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Alumno;
use App\Asignatura;
use App\Record;
use App\Grado;
use App\Attribution;
use Session;
use Auth;
use App\Assignment;
use App\Nota;
use App\Trimestre;
use App\Conducta;

class ConductasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      //for ($i=0; $i < 4; $i++) {
          $asignatura = new Conducta($request->all());
          //$asignatura->promedio = ($asignatura->nota1+$asignatura->nota2+$asignatura->nota3)/3;
          $asignatura->save();


    //  }
      //$asignatura=new Nota($request->all());
      //$asignatura->save();
      $nombre = $asignatura->records->alumnos->nombres;
      $apellido = $asignatura->records->alumnos->apellido_padre;
      $apellidom = $asignatura->records->alumnos->apellido_madre;
      flash()->success('La Conducta del Alumno (a) '.$nombre.' '.$apellido.' '.$apellidom.' ha sido registrada de forma exitosa');

      $var = $asignatura->records->grados->id;
      //$va =$asignatura->asignaturas->id;

      if($asignatura->idtrimestre == 1)
      {
        return redirect()->route('administracion/conductas/editar', ['idgrado' => $var]);
      }elseif ($asignatura->idtrimestre == 2) {
        return redirect()->route('administracion/conductas/edit_conducta', ['idgrado' => $var]);
      }else {
        return redirect()->route('administracion/conductas/edit_conductas', ['idgrado' => $var]);
      }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editar($idgrado) //Primer Trimestre
    {
      $identificador = Grado::find($idgrado);
      //$asig = Asignatura::find($idasignatura);

      //  $asignaciones = Assignment :: with('docentes', 'grados')->orderBy('id', 'ASC')->paginate(20);
      //$conductas = Conducta::orderBy('id', 'ASC')->pluck('nombre', 'id');
      //$asignaturas = Asignatura ::orderBy('id', 'ASC')->pluck('nombre', 'id');
      $registros = Record :: with('alumnos', 'grados')->orderBy('id', 'ASC')->paginate(20);
      $alumnos= Alumno::orderBy('id', 'ASC')->pluck('nombres', 'id');
      //$notas = Nota::orderBy('id', 'ASC');

      return view('administracion.conductas.editar')->with('identificador', $identificador)->with('registros', $registros)->with('alumnos', $alumnos);
    }

    public function edit_conducta($idgrado) //Segundo Trimestre
    {
      $identificador = Grado::find($idgrado);
      //$asig = Asignatura::find($idasignatura);

      //  $asignaciones = Assignment :: with('docentes', 'grados')->orderBy('id', 'ASC')->paginate(20);
      //$conductas = Conducta::orderBy('id', 'ASC')->pluck('nombre', 'id');
      //$asignaturas = Asignatura ::orderBy('id', 'ASC')->pluck('nombre', 'id');
      $registros = Record :: with('alumnos', 'grados')->orderBy('id', 'ASC')->paginate(20);
      $alumnos= Alumno::orderBy('id', 'ASC')->pluck('nombres', 'id');
      //$notas = Nota::orderBy('id', 'ASC');

      return view('administracion.conductas.edit_conducta')->with('identificador', $identificador)->with('registros', $registros)->with('alumnos', $alumnos);
    }

    public function edit_conductas($idgrado) //Tercer Trimestre
    {
      $identificador = Grado::find($idgrado);
      //$asig = Asignatura::find($idasignatura);

      //  $asignaciones = Assignment :: with('docentes', 'grados')->orderBy('id', 'ASC')->paginate(20);
      //$conductas = Conducta::orderBy('id', 'ASC')->pluck('nombre', 'id');
      //$asignaturas = Asignatura ::orderBy('id', 'ASC')->pluck('nombre', 'id');
      $registros = Record :: with('alumnos', 'grados')->orderBy('id', 'ASC')->paginate(20);
      $alumnos= Alumno::orderBy('id', 'ASC')->pluck('nombres', 'id');
      //$notas = Nota::orderBy('id', 'ASC');

      return view('administracion.conductas.edit_conductas')->with('identificador', $identificador)->with('registros', $registros)->with('alumnos', $alumnos);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $identificador=Grado::find($id);
    //  $ident=Alumno::find($id);

      $notas = new Conducta($request->all());
      $notas->fill($request->all());
    //  $registros->$idalumno->asignado=1;
      $notas->save();

      flash()->warning('La asignacion ha sido almacenada con exito');

      return redirect()->route('administracion.conductas.editar', $identificador);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
