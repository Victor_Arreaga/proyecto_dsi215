<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AvanceIndicador;
use App\AreaIndicador;
use App\Indicador;
use App\Assignment;
use App\Grado;
use App\Record;
use App\Alumno;
use App\Http\Requests\AssigIndRequest;
use Illuminate\Support\Facades\DB;
use App\Area1;
use App\Area2;
use App\Area3;
use App\Docente;

class IndAssingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    { 

$carbon = new \Carbon\Carbon();
$date25 = $carbon->now();
$year = $date25->format('Y');

      $a = array(
    NULL => 'Inicio',
    1    => 'Intermedio',
    2    => 'Final');
      $areas = array(
    NULL => 'Desarrollo Personal y Social',
    1    => 'Expresion, Comunicacion y Representacion',
    2    => 'Relacion con el Entorno');

      $avances = array(
    NULL => 'S',
    1    => 'T',
    2    => 'P');

      $area1 = Area1::with('record')->orderBy('id', 'ASC')->paginate(20);
        $area2 = Area2::with('record')->orderBy('id', 'ASC')->paginate(20);
        $area3 = Area3::with('record')->orderBy('id', 'ASC')->paginate(20);


         $asignas = Assignment::with('docentes', 'grados')->orderBy('id', 'ASC')->paginate(20);
        return view('administracion.avancesind.index')->with('a', $a)->with('areas', $areas)->with('asignas', $asignas)->with('avances', $avances)->with('area1', $area1)->with('date25', $date25)->with('year', $year);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

public function cargarRegistros()
    {

    }

public function multipleInsert(Request $request)
    {

        $areass = $request->input('identificador');

        if ($areass == 'Desarrollo Personal y Social'){


        $data = $request->except('_token');
        $data2 = $request->input('level');
        
            $subject_count = count($data['hidden']);
        for($i=0; $i < $subject_count; $i++){  
           $count = count($data['avance1']);
           for($j=0; $j < $count; $j++){ 
            if($i==$j){
                $idarea = $data['hidden'][$i];



                $area1=Area1::where('id', '=', $idarea)->first();
                if(count($area1)>=1){

      $area1->I1 = $data['avance1'][$i];
      $area1->I2 = $data['avance2'][$i];
      $area1->I3 = $data['avance3'][$i];
      $area1->I4 = $data['avance4'][$i];
      $area1->I5 = $data['avance5'][$i];
      $area1->I6 = $data['avance6'][$i];
      $area1->I7 = $data['avance7'][$i];
      $area1->I8 = $data['avance8'][$i];
      $area1->I9 = $data['avance9'][$i];
      $area1->I10 = $data['avance10'][$i];
      $area1->I11 = $data['avance11'][$i];
      $area1->I12 = $data['avance12'][$i];
      $area1->I13 = $data['avance13'][$i];
      $area1->I14 = $data['avance14'][$i];
      $area1->I15 = $data['avance15'][$i];
      $area1->I16 = $data['avance16'][$i];
      $area1->I17 = $data['avance17'][$i];
      $area1->I18 = $data['avance18'][$i];
      $area1->I19 = $data['avance19'][$i];
      $area1->I20 = $data['avance20'][$i];      
      $area1->I21 = $data['avance21'][$i];

      // Guardamos en base de datos
   $area1->update();
}
     
  }
}
}

   return redirect()->action('IndAssingController@index');
}

elseif ($areass == 'Expresion, Comunicacion y Representacion'){
    $data = $request->except('_token');
        $data2 = $request->input('level');
        
            $subject_count = count($data['hidden']);
        for($i=0; $i < $subject_count; $i++){  
           $count = count($data['avance1']);
           for($j=0; $j < $count; $j++){ 
            if($i==$j){
                $idarea = $data['hidden'][$i];



                $area1=Area2::where('id', '=', $idarea)->first();
                if(count($area1)>=1){

      $area1->I1 = $data['avance1'][$i];
      $area1->I2 = $data['avance2'][$i];
      $area1->I3 = $data['avance3'][$i];
      $area1->I4 = $data['avance4'][$i];
      $area1->I5 = $data['avance5'][$i];
      $area1->I6 = $data['avance6'][$i];
      $area1->I7 = $data['avance7'][$i];
      $area1->I8 = $data['avance8'][$i];
      $area1->I9 = $data['avance9'][$i];
      $area1->I10 = $data['avance10'][$i];
      $area1->I11 = $data['avance11'][$i];
      $area1->I12 = $data['avance12'][$i];
      $area1->I13 = $data['avance13'][$i];
      $area1->I14 = $data['avance14'][$i];
      $area1->I15 = $data['avance15'][$i];

      // Guardamos en base de datos
   $area1->update();
}
     
  }
}
}

   return redirect()->action('IndAssingController@index');


}

elseif ($areass == 'Relacion con el Entorno'){
    $data = $request->except('_token');
        $data2 = $request->input('level');
        
            $subject_count = count($data['hidden']);
        for($i=0; $i < $subject_count; $i++){  
           $count = count($data['avance1']);
           for($j=0; $j < $count; $j++){ 
            if($i==$j){
                $idarea = $data['hidden'][$i];



                $area1=Area3::where('id', '=', $idarea)->first();
                if(count($area1)>=1){

      $area1->I1 = $data['avance1'][$i];
      $area1->I2 = $data['avance2'][$i];
      $area1->I3 = $data['avance3'][$i];
      $area1->I4 = $data['avance4'][$i];
      $area1->I5 = $data['avance5'][$i];
      $area1->I6 = $data['avance6'][$i];
      $area1->I7 = $data['avance7'][$i];
      $area1->I8 = $data['avance8'][$i];
      $area1->I9 = $data['avance9'][$i];
      $area1->I10 = $data['avance10'][$i];
      $area1->I11 = $data['avance11'][$i];
      $area1->I12 = $data['avance12'][$i];
      $area1->I13 = $data['avance13'][$i];
      $area1->I14 = $data['avance14'][$i];
      $area1->I15 = $data['avance15'][$i];

      // Guardamos en base de datos
   $area1->update();
}
     
  }
}
}

   return redirect()->action('IndAssingController@index');


}

    }


    public function cargarFormulario(Request $request)
    {
        $data = $request->except('_token');
        $grade = $request->input('grade');
        $id = $request->input('docente');
        $docente=Docente::find($id);
        $nombre=$docente->nombres;
        $apellidos = $docente->apellidos;
        $records = Record::all()->where("idgrado","=",$grade);
        $area = $request->input('area');
        $nivel = $request->input('level');

        if ($area == 'Desarrollo Personal y Social'){
        foreach ($records as $record){
                        $area1 = new Area1;
                        $area1->idrecord = $record->id;
                        $area1->nivel = $request->input('level');
                        $carbon = new \Carbon\Carbon();
                        $date25 = $carbon->now();
                        $year = $date25->format('Y');
                        $area1->anio = $year;

                       $area1_favorites = DB::table('area1s')
    ->where('idrecord', '=', $record->id)
    ->where('nivel', '=', $request->input('level'))
    ->first();
    if (is_null($area1_favorites)) {
        $area1->save();
        
} else {  

}

}

return redirect()->route('indicador_assignment.registrar',[$area,$nombre, $apellidos, $nivel]);
}
elseif ($area == 'Expresion, Comunicacion y Representacion'){


        foreach ($records as $record){
                        $area1 = new Area2;
                        $area1->idrecord = $record->id;
                        $area1->nivel = $request->input('level');
                        $carbon = new \Carbon\Carbon();
                        $date25 = $carbon->now();
                        $year = $date25->format('Y');
                        $area1->anio = $year;

                       $area1_favorites = DB::table('area2s')
    ->where('idrecord', '=', $record->id)
    ->where('nivel', '=', $request->input('level'))
    ->first();
    if (is_null($area1_favorites)) {
        $area1->save();
        
} else {
   

}
                 

}

return redirect()->route('indicador_assignment.registrar',[$area,$nombre, $apellidos, $nivel]);


}
elseif ($area == 'Relacion con el Entorno'){


        foreach ($records as $record){
                        $area1 = new Area3;
                        $area1->idrecord = $record->id;
                        $area1->nivel = $request->input('level');
                        $carbon = new \Carbon\Carbon();
                        $date25 = $carbon->now();
                        $year = $date25->format('Y');
                        $area1->anio = $year;

                       $area1_favorites = DB::table('area3s')
    ->where('idrecord', '=', $record->id)
    ->where('nivel', '=', $request->input('level'))
    ->first();
    if (is_null($area1_favorites)) {
        $area1->save();
        
} else {
   

}
                 

}

return redirect()->route('indicador_assignment.registrar',[$area,$nombre, $apellidos, $nivel]);


}

    }


public function show($id)
    {
        
    }
    
    public function edit($id)

    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    }
 

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }

public function registrar($area,$doc,$app,$nivel)

    {
        $data0 = $doc;
        $data1 = $app;
        $data2 = $nivel;
        $data3 = $area;

        $carbon = new \Carbon\Carbon();
$date25 = $carbon->now();
$year = $date25->format('Y');    
               
if ($data3 == 'Desarrollo Personal y Social'){
          $area1 = Area1::with('record')->orderBy('id', 'ASC')->paginate(20);
        return view('administracion.avancesind.area1')->with('area1', $area1)->with('year', $year)->with('data2',$data2)->with('data0',$data0)->with('data1',$data1)->with('data3',$data3);
    }
    elseif ($data3 == 'Expresion, Comunicacion y Representacion')
        {
            $area1 = Area2::with('record')->orderBy('id', 'ASC')->paginate(20);
            return view('administracion.avancesind.area2')->with('area1', $area1)->with('date25', $date25)->with('year', $year)->with('data2',$data2)->with('data0',$data0)->with('data1',$data1)->with('data3',$data3);
        }
   
        else {
            $area1 = Area3::with('record')->orderBy('id', 'ASC')->paginate(20);
             return view('administracion.avancesind.area3')->with('area1', $area1)->with('date25', $date25)->with('year', $year)->with('data2',$data2)->with('data0',$data0)->with('data1',$data1)->with('data3',$data3);

        }


    }


        
}
