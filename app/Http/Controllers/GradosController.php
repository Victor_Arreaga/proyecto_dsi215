<?php

namespace App\Http\Controllers;
use App\Grado;
use App\Turno;
use App\Http\Requests\GradoRequest;

use Illuminate\Http\Request;

class GradosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $grados= Grado::orderBy('id', 'ASC')->paginate(250);
      return view('administracion.grados.index')->with('grados', $grados);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      //  return view('administracion.grados.create');
      $turnos= Turno::orderBy('id', 'ASC')->pluck('turno', 'id');

      return view('administracion.grados.create')->with('turnos', $turnos);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(GradoRequest $request)
    {
      $grado=new Grado($request->all());
      $grado->save();
      flash()->success('Se ha registrado la asignatura de forma exitosa');

      return redirect()->route('grados.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $grado = Grado::find($id);
      $grado->turno;

      $turnos = Turno::orderBy('id', 'ASC')->pluck('turno', 'id');
      return view('administracion.grados.edit')
          ->with('turnos', $turnos)
          ->with('grado', $grado);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $grado = Grado::find($id);
      $grado->fill($request->all());
      $grado->save();
      flash()->success('Los datos del grado han sido actulaizados con exito');

      return redirect()->route('grados.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   public function destroy($id)
    {
        $grado = Grado::find($id);
        $grado->activo=0;
        $grado->save();
        flash()->success('El grado '.$grado->nombre.' ha sido dado de baja');
        return redirect()->route('grados.index');
    }

    public function bajasgindex()
    {
         $grados= Grado::orderBy('id', 'ASC')->paginate(250);
         return view('administracion.grados.baja')->with('grados', $grados);
    }

    public function altag($id)
    {
        $grado = Grado::find($id);
        $grado->activo=1;
        $grado->save();
        flash()->success('El grado '.$grado->nombre.' ha sido dado de alta');
        return redirect()->route('grados.index');
          
    }
}
