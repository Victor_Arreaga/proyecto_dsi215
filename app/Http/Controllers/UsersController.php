<?php

namespace App\Http\Controllers;
use App\User;
//use Illuminate\Validation\Validator;
use Hash;
use Validator;
use Auth;
use App\Docente;
use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;

class UsersController extends Controller
{

    public function __construct()    //CONSTRUCTOR UNICAMENTE PARA VALIDAD QUE
    {                                //EL USUARIO QUE INGESE DEBE ESTAE REGISTRADO Y SER DIRECTOR

        $this->middleware('admin');
    }

    public function index(Request $request)
    {


        $users = User::with('docente')->orderBy('id', 'ASC')->paginate(250);

        return view('administracion.usuarios.index')->with('users', $users);
    }

    public function create()
    {
        $docentes = Docente::orderBy('nip', 'ASC')->pluck('nombres', 'id');

        return view('administracion.usuarios.create')->with('docentes', $docentes);
    }

    public function store(UserRequest $request)
    {
        $user = new User($request->all());
        $user->password = bcrypt($request->password);
        $user->save();

        flash()->success('Se ha registrado el usuario de forma exitosa');

        return redirect()->route('usuarios.index');
    }

    public function show($id)
    {
    }

    public function edit($id)
    {
        $user = User::find($id);
        $user->docente;

        $docentes = Docente::orderBy('nip', 'ASC')->pluck('nombres', 'id');
        return view('administracion.usuarios.edit')
            ->with('docentes', $docentes)
            ->with('user', $user);
    }


    public function update(Request $request, $id)
    {
        $user = User::find($id);
        $user->fill($request->all());
        $user->save();
        flash()->success('Los datos del usuario han sido actulaizados con exito');

        return redirect()->route('usuarios.index');
    }

    public function destroy($id)
    {
        $usuario = User::find($id);
        $usuario->activo=0;
        $usuario->save();
        flash()->success('El usuario '.$usuario->docente->nombres.' ha sido dado de baja');
        return redirect()->route('usuarios.index');
    }

    public function bajasuindex()
    {
        $users = User::with('docente')->orderBy('id', 'ASC')->paginate(10);

        return view('administracion.usuarios.bajas')->with('users', $users);
    }

    public function altau($id)
    {
        $usuario = User::find($id);
        $usuario->activo=1;
        $usuario->save();
        flash()->success('El usuario '.$usuario->docente->nombres.' ha sido dado de alta');
        return redirect()->route('usuarios.index');
          
    }

}