<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Docente;
use App\Http\Requests\DocenteRequest;

class DocentesController extends Controller
{

 public function __construct()  //CONSTRUCTOR UNICAMENTE PARA VALIDAD QUE
    {                           //EL USUARIO QUE INGESE DEBE ESTAE REGISTRADO Y SER DIRECTOR
      $this->middleware('admin');
    }

    public function index(){
        $docentes= Docente::orderBy('id', 'ASC')->paginate(250);

        return view('administracion.docentes.index')->with('docentes', $docentes);
    }

    public function create(){
    	return view('administracion.docentes.create');
    }

    public function store(DocenteRequest $request){
        $user=new Docente($request->all());
        $user->save();
        flash()->success('Se ha registrado al docente de forma exitosa');

        return redirect()->route('docentes.index');
    }

    public function show($id){

    }

    public function edit($id){
    	$docente=Docente::find($id);
        return view('administracion.docentes.edit')->with('docente', $docente);

    }

    public function update(Request $request, $id){
    	$docente=Docente::find($id);
        $docente->fill($request->all());
        $docente->save();

        flash()->success('Los datos del docente han sido actulaizados con exito');

        return redirect()->route('docentes.index');
    }

   public function destroy($id)
    {
        $docente = Docente::find($id);
        $docente->activo=0;
        $docente->save();
        flash()->success('El docente '.$docente->nombres.' ha sido dado de baja');
        return redirect()->route('docentes.index');
    }

    public function bajasuindex()
    {
        $docentes = Docente::with('docente')->orderBy('id', 'ASC')->paginate(10);

        return view('administracion.docentes.bajas');
    }

    public function altad($id)
    {
        $docente = Docente::find($id);
        $docente->activo=1;
        $docente->save();
        flash()->success('El docente '.$docente->nombres.' ha sido dado de alta');
        return redirect()->route('docentes.index');
          
    }
}
