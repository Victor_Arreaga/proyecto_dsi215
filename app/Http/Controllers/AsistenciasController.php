<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Alumno;
use App\Record;
use App\Grado;
use App\Asistencia;
use Session;
use Auth;
use App\Http\Requests\RecordRequest;
use App\Http\Requests\AsistenciaRequest;
use App\Assignment;



class AsistenciasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$registros = Record::with('alumnos', 'grados')->orderBy('id', 'ASC')->paginate(20);

      //$registros =Record::search($request->idgrado)->orderBy('id', 'ASC')->paginate(10);
      //$idgrado="Cuarto A";

      $registros = Assignment::with('docentes', 'grados')->orderBy('id', 'ASC')->paginate(250);

      return view('administracion.asistencias.index')->with('registros', $registros)->with('registros', $registros);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($idgrado)
    {
        $identificador = Grado::find($idgrado);
        $grado = $idgrado;

     //  $asignaciones = Assignment :: with('docentes', 'grados')->orderBy('id', 'ASC')->paginate(20);
       $registros = Record :: with('alumnos', 'grados')->orderBy('id', 'ASC')->paginate(250);
       $alumnos= Alumno::orderBy('id', 'ASC')->pluck('nombres', 'id');

       return view('administracion.asistencias.edit')->with('identificador', $identificador)->with('registros', $registros)->with('alumnos', $alumnos)->with('grado', $idgrado);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function insertarAsistencia(AsistenciaRequest $request)
    {
           

    try {
             

            $dato=$request->except('_token');
            $grados=$request->input('grado');
            $registro= count($dato['record']);

            for ($i=0; $i < $registro; $i++)
            {
                $estado=count($dato['estado']);

                for( $j=0; $j < $estado; $j++)
                {
                    if($i==$j)
                    {
                      $idrecord=$dato['record'][$i];
                      
                     // $asistencia = Asistencia:: where('id', '=', $idrecord);
                      //if (count($asistencia)>=1)
                      //{


                        $asistencia=new Asistencia;

                        $asistencia->idrecord=$dato['record'][$i];
                        $asistencia->estado=$dato['estado'][$i];
                        $asistencia->fecha=$dato['fecha'];

                        $asistencia->save();
                      //}  
                    }
                }
            }

        }catch (\Illuminate\Database\QueryException $e) {

               flash()->error('No se puede registrar la asistencia porque la fecha ya esta en uso');
               return redirect()->action('AsistenciasController@edit', $grados);

            }

            flash()->success('Se ha registrado la asistencia de manera exitosa');
            return redirect()->action('AsistenciasController@edit', $grados);


    }
}
