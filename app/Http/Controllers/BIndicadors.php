<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AreaIndicador;
use App\Indicador;

class BIndicadors extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $indicadors = Indicador::with('area_indicador')->orderBy('id', 'ASC')->paginate(250);

        return view('administracion.indicadores.bajaindex')->with('indicadors', $indicadors);
   
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $indicadors = Indicador::find($id);
        $indicadors->area_indicador;

        $area_indicadors = AreaIndicador::orderBy('id', 'ASC')->pluck('nombre', 'id');
        return view('administracion.indicadores.alta')
            ->with('area_indicadors', $area_indicadors)
            ->with('indicadors', $indicadors);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $indicadors = Indicador::find($id);
        $indicadors->fill($request->all());         
            $indicadors->save();
            
     flash()->warning('El Registro del Area '. $indicadors->nombre. ' ha sido Activado');

        return redirect()->route('indicadors.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
