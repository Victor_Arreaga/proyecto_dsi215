<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AlumnoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

        'nombres'                 => 'required|min:3|max:40|regex:/^[a-z\sáÁéÉíóú]+$/i',
        'apellido_padre'          => 'required|min:3|max:12|regex:/^[a-z\sáÁéÉíóú]+$/i',
        'apellido_madre'          => 'required|min:3|max:12|regex:/^[a-z\sáÁéÉíóú]+$/i',
        'sexo'                    => 'required|min:8|max:9|',
        'fecha_nacimiento'        => 'required|min:10|max:10|',
        'repite'                  => 'required|min:2|max:2|',
        'estudio_parvularia'      => 'required|min:2|max:2|',
        'zona_residencia'         => 'required|min:5|max:6|',
        'cod_depto_residencia'    => 'numeric|required|digits_between:4,4',
        'actividad_econ'          => 'required|min:2|max:2|',
        'tipos_discapacidad'      => 'required|min:5|max:100|regex:/^[a-z\sáéíóú]+$/i',
        'encargado'               => 'required|min:5|max:50|regex:/^[a-z\sáéíóú]+$/i',
        'direccion'               => 'required|min:5|max:100|regex:/^[a-z\sáéíóú]+$/i',


        ];
    }


    public function messages(){
        return [
          /*  'nie.required' => 'El campo NIE es requerido',
            'nie.numeric' => 'El campo NIE solo acepta numeros',
            'nie.min' => 'El mínimo permitido para NIE son 6 caracteres',
            'nie.max' => 'El máximo permitido para NIE son 8 caracteres',

            'nombres.required' => 'El campoNombre es requerido',
            'nombres.min' => 'El mínimo permitido para Nombres son 6 caracteres',
            'nombres.max' => 'El maximo permitido para Nombres son 6 caracteres',
            'nie.regex' => 'El campo Nombres solo acepta letras',

            'responsable.required' => 'El campo Responsable es requerido',
            'responsable.min' => 'El mínimo permitido para Responsable son 6 caracteres',
            'responsable.max' => 'El maximo permitido para Responsable son 6 caracteres',
            'responsable.regex' => 'El campo Responsable solo acepta letras',
*/


        ];
    }
}
