<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Asistencia extends Model
{
    protected $fillable =['idrecord','estado', 'fecha'];

    public function alumnos(){
        return $this->hasMany('App\Alumno');
    }
}
