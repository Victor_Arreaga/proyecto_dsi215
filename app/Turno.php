<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Turno extends Model
{
    protected $fillable =['turno'];

    public function grados(){
    	return $this->hasMany('App\Grado');
    }
}
