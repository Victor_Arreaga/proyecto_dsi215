<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Area3 extends Model
{
    protected $fillable = [
        'idrecord', 'I1', 'I2', 'I3','I4', 'I5', 'I6','I7', 'I8', 'I9','I10', 'I11', 'I12','I13', 'I14', 'I15', 'nivel','anio'
    ];   


    public function record(){
        return $this->belongsTo('App\Record', 'idrecord');
    }
}
