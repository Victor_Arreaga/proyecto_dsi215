<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'iddocente', 'email', 'rol', 'password'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function docente(){
        return $this->belongsTo('App\Docente', 'iddocente');
    }

    public function scopeSearch($query, $rol)
    {
        return $query->where('rol', 'LIKE', "%$rol%");
    }
}
