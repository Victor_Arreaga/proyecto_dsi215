<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Asignacion extends Model
{
    //public $table = "asignacion";
    protected $fillable=['iddocente', 'idgrado', 'idalumno'];

    public function docentes(){
        return $this->belongsTo('App\Docente');
    }

    public function grados(){
        return $this->belongsTo('App\Grado');
    }
}
