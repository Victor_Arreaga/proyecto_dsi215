<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Trimestre extends Model
{
  protected $fillable =['nombre'];

  public function notas(){
      return $this->hasMany('App\Nota');
  }

  public function conductas(){
      return $this->belongsTo('App\Conducta');
  }
}
