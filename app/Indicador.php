<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Indicador extends Model
{
    protected $fillable = [
      'id_area','nombre', 'activo','Nivel'
  ];

  public function area_indicador(){
        return $this->belongsTo('App\AreaIndicador', 'id_area');
    }

    public function avance_indicadores(){
        return $this->hasMany('App\AvanceIndicador');
    }
}
